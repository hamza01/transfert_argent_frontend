import { combineReducers } from "redux";
import app from "../app/containers/App/appReducer";
import { routerReducer } from "react-router-redux";
import { reducer as reduxAsyncConnect } from "redux-connect";
import { reducer as form } from "redux-form";
import { loadingBarReducer } from "react-redux-loading-bar";
import user from "../app/containers/User/UserReducer";
import loginReducer from "../app/containers/Login/loginReducer";

export default combineReducers({
  app,
  routing: routerReducer,
  reduxAsyncConnect,
  loadingBar: loadingBarReducer,
  form,
  user,
  loginReducer,
});
