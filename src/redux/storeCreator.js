import { createStore as _createStore, applyMiddleware, compose } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import { routerMiddleware } from "react-router-redux";
import createMiddleware from "./clientMiddleware";
import reducer from "./reducer";
import { loadingBarMiddleware } from "react-redux-loading-bar";

const storeCreator = (client) => {
  const middleware = [
    createMiddleware(client),
    loadingBarMiddleware({
      promiseTypeSuffixes: ["REQUEST", "SUCCESS", "FAIL"],
    }),
  ];
  let finalCreateStore = "";
  if (__DEVELOPMENT__ && __CLIENT__ && __DEVTOOLS__) {
    finalCreateStore = compose(
      composeWithDevTools(applyMiddleware(...middleware))
    )(_createStore);
  } else {
    finalCreateStore = applyMiddleware(...middleware)(_createStore);
  }
  const store = finalCreateStore(reducer);
  if (__DEVELOPMENT__ && module.hot) {
    module.hot.accept("./reducer", () => {
      store.replaceReducer(require("./reducer"));
    });
  }
  return store;
};
// Store Creator By Client
export default storeCreator;
