const environment = {
  development: {
    isProduction: false,
  },
  production: {
    isProduction: true,
  },
}[process.env.NODE_ENV || "development"];

module.exports = Object.assign(
  {
    host: process.env.HOST || "localhost",
    port: process.env.PORT,
    // apiHost: process.env.APIHOST || '192.168.1.125',
    apiHost: process.env.APIHOST || "localhost",
    // apiPort: process.env.APIPORT,
    apiPort: 9966,
    app: {
      title: "Adria Direct Banking",
      description: "Adria Cross Channel Direct Banking",
      head: {
        titleTemplate: "Adria Direct Banking: %s",
        meta: [
          {
            name: "description",
            content: "Adria Cross Channel Direct Banking application.",
          },
          { charset: "utf-8" },
          { property: "og:site_name", content: "Adria Direct Banking" },
          {
            property: "og:image",
            content:
              "http://adria-bt.com/wp-content/themes/adria/images/logo.png",
          },
          { property: "og:locale", content: "en_US" },
          { property: "og:title", content: "Adria Direct Banking" },
          {
            property: "og:description",
            content: "Adria Cross Channel Direct Banking application.",
          },
          { property: "og:card", content: "summary" },
          { property: "og:site", content: "@adriatechnology" },
          { property: "og:creator", content: "@adriatechnology" },
          { property: "og:image:width", content: "200" },
          { property: "og:image:height", content: "200" },
          { property: "og:addressSpring", content: "192.168.1.125" },
          { property: "og:portSpring", content: "9966" },
        ],
      },
    },
  },
  environment
);
