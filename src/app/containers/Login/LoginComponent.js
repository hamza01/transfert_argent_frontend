import React, { Component } from "react";
import { FormControl } from "react-bootstrap";
import { withNamespaces } from "react-i18next";
import Scrollchor from "react-scrollchor";
import { reduxForm } from "redux-form";
import Select from "react-select";
import localStorage from "local-storage";
import Helmet from "react-helmet";

const validate = (values) => {
  const errors = {};
  if (!values.username) {
    errors.username = "errors.requiredField";
  }
  if (!values.password) {
    errors.password = "errors.requiredField";
  }
  return errors;
};

class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      showKeyboard: false,
      matrice: [
        7,
        "",
        4,
        "",
        2,
        "",
        "",
        "",
        9,
        "",
        1,
        "",
        "",
        5,
        "",
        "",
        6,
        0,
        "",
        "",
        8,
        "",
        "",
        "",
        3,
      ],
      codeLangue: props.codeLangue,
    };
  }

  handlePasswordChange = (event) => {
    this.setState({ password: event.target.value });
  };

  appendValue = (key, value) => {
    this.setState({ password: this.state.password.concat(value) });
  };

  clearPassword = () => {
    this.setState({ password: "" });
  };

  shuffleMatrix = (array) => {
    let currentIndex = array.length,
      temporaryValue,
      randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  };

  showKeyboard = () => {
    this.setState({
      showKeyboard: true,
      matrice: this.shuffleMatrix(this.state.matrice),
    });
  };

  enterDemoMode = () => {
    localStorage.set("isDemo", true);
    this.props.auth({});
  };

  render() {
    const styles = require("../../../themes/00000/containers/login.scss");
    const fr = require("../../../themes/00000/images/world_flags/fr.png");
    const uk = require("../../../themes/00000/images/world_flags/gb.png");
    const sa = require("../../../themes/00000/images/world_flags/sa.png");
    const es = require("../../../themes/00000/images/world_flags/es.png");
    const cn = require("../../../themes/00000/images/world_flags/cn.png");
    const pt = require("../../../themes/00000/images/world_flags/pt.png");

    const {
      t,
      fields: { username, password },
      values,
      auth,
      virtualKeyboardEnabled,
      alreadyConnected,
      messageStatut,
    } = this.props;

    return (
      <div id="auth_PAGE" style={{ background: "#fff" }}>
        <Helmet title={"Adria direct banking"} />
        <div
          className={"navbar sticky " + styles.navbarCustom}
          role="navigation"
        >
          <div className="container">
            <div className="navbar-header">
              <button
                className="navbar-toggle"
                data-target=".navbar-collapse"
                data-toggle="collapse"
                type="button"
              >
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>

              <a
                className="navbar-brand logo"
                href="#"
                style={{ padding: "7px" }}
              >
                {/* <img
                  src={require("../../../themes/00000/images/app-logo.png")}
                  style={{ height: "56px" }}
                /> */}
              </a>
            </div>

            <div className="navbar-collapse collapse" id="navbar-menu">
              <div
                className="pull-right"
                style={{ margin: "20px 0 20px 20px" }}
              >
                <div className="lang-selector" style={{ width: "80px" }}>
                  <Select
                    onChange={this.props.handleLangChange}
                    options={[
                      {
                        value: "fr",
                        label: (
                          <div style={{ fontWeight: "bold" }}>
                            <img
                              src={fr}
                              style={{ height: "12px", marginRight: "5px" }}
                            />{" "}
                            FR
                          </div>
                        ),
                      },
                      {
                        value: "en",
                        label: (
                          <div style={{ fontWeight: "bold" }}>
                            <img
                              src={uk}
                              style={{ height: "12px", marginRight: "5px" }}
                            />{" "}
                            EN
                          </div>
                        ),
                      },
                      {
                        value: "es",
                        label: (
                          <div style={{ fontWeight: "bold" }}>
                            <img
                              src={es}
                              style={{ height: "12px", marginRight: "5px" }}
                            />{" "}
                            ES
                          </div>
                        ),
                      },
                      {
                        value: "pt",
                        label: (
                          <div style={{ fontWeight: "bold" }}>
                            <img
                              src={pt}
                              style={{ height: "12px", marginRight: "5px" }}
                            />{" "}
                            PT
                          </div>
                        ),
                      },
                      {
                        value: "ar",
                        label: (
                          <div style={{ fontWeight: "bold" }}>
                            <img
                              src={sa}
                              style={{ height: "12px", marginRight: "5px" }}
                            />{" "}
                            AR
                          </div>
                        ),
                        disabled: true,
                      },
                      {
                        value: "cn",
                        label: (
                          <div style={{ fontWeight: "bold" }}>
                            <img
                              src={cn}
                              style={{ height: "12px", marginRight: "5px" }}
                            />{" "}
                            CN
                          </div>
                        ),
                        disabled: true,
                      },
                    ]}
                    value={this.state.codeLangue}
                    searchable={false}
                    clearable={false}
                    removeSelected
                  />
                </div>
              </div>
              <ul className="nav navbar-nav  navbar-right">
                <li className="active">
                  <Scrollchor to="#home">{t("titles.home")}</Scrollchor>
                </li>
                <li>
                  <Scrollchor to="#features">{t("titles.services")}</Scrollchor>
                </li>
                <li>
                  <Scrollchor to="#appMobile">
                    {t("titles.download")}
                  </Scrollchor>
                </li>
                <li>
                  <Scrollchor to="#faq">{t("titles.faq")}</Scrollchor>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <section className={styles.home} id="home">
          <div className={styles.elementorShape} data-negative="true">
            <svg
              preserveAspectRatio="none"
              viewBox="0 0 1000 100"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M0.2,94.7L0,0v100h1000V0L0.2,94.7z"
                style={{ fill: "#fff" }}
              />
            </svg>
          </div>

          <div className="container">
            <div className="row">
              <div className="col-lg-6 col-md-6 col-xs-12">
                <div className={styles.homeWrapper}>
                  <h2>{t("login.intro1")}</h2>
                  <p>{t("login.intro2")}</p>
                  <button
                    className={"btn " + styles.loginBtn}
                    onClick={this.enterDemoMode}
                    style={{ width: "auto", padding: "8px 20px" }}
                  >
                    Démonstration
                  </button>
                </div>
              </div>

              <div
                className={
                  "col-lg-4 col-lg-offset-2 col-md-5 col-md-offset-1 col-xs-12 " +
                  styles.introForm
                }
                style={{
                  boxShadow: "1px 1px 1px #e3e3e3",
                  border: "1px solid #ddd",
                }}
              >
                <div className={styles.homeWrapper}>
                  <div className={styles.introForm} style={{ margin: "0" }}>
                    <h3 className="text-center"> {t("login.logIn")} </h3>

                    {alreadyConnected && (
                      <div
                        className="text-danger"
                        style={{ paddingBottom: "10px" }}
                      >
                        <i className="fa fa-exclamation-triangle" />{" "}
                        {t("login.alreadyConnected")}
                      </div>
                    )}

                    {messageStatut && (
                      <div
                        className="text-danger"
                        style={{ paddingBottom: "10px" }}
                      >
                        <i className="fa fa-exclamation-triangle" />{" "}
                        {messageStatut}
                      </div>
                    )}

                    <div className="form-group">
                      <div className={styles.unWrap}>
                        <span>
                          <i className="fa fa-user-o" />
                        </span>
                        <FormControl
                          {...username}
                          id="username_TXTF"
                          name="username"
                          placeholder="Username"
                          type="text"
                        />
                      </div>
                      {username.touched && username.error && (
                        <div className="text-danger">
                          <i className="fa fa-exclamation-triangle" />{" "}
                          {t(username.error)}
                        </div>
                      )}
                    </div>
                    <div className="form-group">
                      <div
                        className={styles.unWrap}
                        style={{ position: "relative", width: "auto" }}
                      >
                        <span>
                          <i className="fa fa-key" />
                        </span>
                        <button
                          className={styles.deleteInput}
                          id="delete_input"
                          onClick={this.clearPassword}
                          style={{ float: "right" }}
                        >
                          &times;
                        </button>
                        <FormControl
                          id="password_TXTF"
                          name="password"
                          onChange={this.handlePasswordChange}
                          onClick={this.showKeyboard}
                          placeholder="Password"
                          readOnly
                          type="password"
                          value={this.state.password}
                        />
                        {this.state.showKeyboard && (
                          <div
                            className={styles.btnMatrix}
                            style={{ display: "table", margin: "20px auto 0" }}
                          >
                            {this.state.matrice &&
                              this.state.matrice.length &&
                              this.state.matrice.map((element) => (
                                <button
                                  disabled={element === ""}
                                  onClick={() => {
                                    this.appendValue(element.key, element);
                                  }}
                                >
                                  {element}
                                </button>
                              ))}
                          </div>
                        )}
                      </div>
                    </div>
                    <div className="form-group text-right">
                      <button
                        className={"btn " + styles.loginBtn}
                        disabled={this.state.password === "" || username.error}
                        id="submit_BTN"
                        onClick={() => {
                          values.password = this.state.password;
                          auth(values);
                        }}
                      >
                        {t("login.login")}
                      </button>
                    </div>
                    <div className="form-group">
                      <a
                        href={
                          "/app/forgotPassword?cl=" + lang + "&cb=" + bankCode
                        }
                        style={{ color: "#999", fontSize: "11px" }}
                      >
                        {t("login.forgotPassword")}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section
          id="features"
          style={{ paddingTop: "60px", paddingBottom: "120px" }}
        >
          <div className="container" id="features-list">
            <div className="row">
              <div className="col-sm-12 text-center">
                <div className={styles.titleBox}>
                  <p className={styles.titleAlt}>{t("login.services")}</p>
                  <h3>{t("login.title")}</h3>
                  <div className={styles.border} />
                </div>
              </div>
            </div>

            <div className="row text-center">
              <div className="col-sm-4" id="Feature1">
                <div className={styles.serviceItem}>
                  <img
                    src={require("../../../themes/00000/images/login/icons/wallet.png")}
                  />
                  <div className={styles.serviceDetail}>
                    <h5>{t("login.manageAcc")}</h5>
                  </div>
                </div>
              </div>

              <div className="col-sm-4" id="Feature2">
                <div className={styles.serviceItem}>
                  <img
                    src={require("../../../themes/00000/images/login/icons/economy.png")}
                  />
                  <div className={styles.serviceDetail}>
                    <h5>{t("login.transferMoney")}</h5>
                  </div>
                </div>
              </div>

              <div className="col-sm-4" id="Feature3">
                <div className={styles.serviceItem}>
                  <img
                    src={require("../../../themes/00000/images/login/icons/online-banking.png")}
                  />
                  <div className={styles.serviceDetail}>
                    <h5>{t("login.receiveAlerts")}</h5>
                  </div>
                </div>
              </div>
            </div>

            <div className="row text-center">
              <div className="col-sm-4" id="Feature4">
                <div className={styles.serviceItem}>
                  <img
                    src={require("../../../themes/00000/images/login/icons/mobile-banking.png")}
                  />
                  <div className={styles.serviceDetail}>
                    <h5>{t("login.bankAnywhere")}</h5>
                  </div>
                </div>
              </div>

              <div className="col-sm-4" id="Feature5">
                <div className={styles.serviceItem}>
                  <img
                    src={require("../../../themes/00000/images/login/icons/budget.png")}
                  />
                  <div className={styles.serviceDetail}>
                    <h5>{t("login.manageinvestments")}</h5>
                  </div>
                </div>
              </div>

              <div className="col-sm-4" id="Feature6">
                <div className={styles.serviceItem}>
                  <img
                    src={require("../../../themes/00000/images/login/icons/file.png")}
                  />
                  <div className={styles.serviceDetail}>
                    <h5>{t("login.payBankOnline")}</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div
            className={styles.elementorShape}
            data-negative="true"
            style={{
              position: "inherit",
              marginTop: "80px",
              marginBottom: "-125px",
              background: "#fff",
            }}
          >
            <svg
              preserveAspectRatio="none"
              viewBox="0 0 1000 100"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M1000.2,94.7L0,0v100h0V0L0.2,94.7z"
                style={{ fill: "#f0f0f0" }}
              />
            </svg>
          </div>
        </section>
        <section
          className={styles.appMobile}
          id="appMobile"
          style={{
            paddingTop: "0",
            paddingBottom: "100px",
            background: "#f0f0f0",
          }}
        >
          <div className="container">
            <div className="row">
              <div className="col-md-4 text-center">
                <img
                  alt="Crédit du Maroc Mobile"
                  className={styles.screen}
                  height="300"
                  src={require("../../../themes/00000/images/login/AppMobile.png")}
                />
              </div>
              <div className="col-md-8 text-center">
                <div className="row" style={{ marginTop: "40px" }}>
                  <div className="col-md-12 text-center">
                    <div>
                      <h3>
                        {t("login.downloadtitle")}
                        <br /> {t("login.title2")}
                      </h3>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12 text-center">
                    <div>
                      <a className={styles.playstore} href="#" target="_blank">
                        <img
                          alt="Play store"
                          height="50"
                          src={require("../../../themes/00000/images/login/playstore.png")}
                        />
                      </a>
                      <a className={styles.appstore} href="#" target="_blank">
                        <img
                          alt="App store"
                          height="50"
                          src={require("../../../themes/00000/images/login/appstore.png")}
                        />
                      </a>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12 text-center">
                    <div>
                      <h4>
                        {t("login.title3")} <br />
                        {t("login.title4")}
                      </h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="faq" style={{ padding: "100px 0 80px" }}>
          <div className="container">
            <div className="row">
              <div className="col-sm-12 text-center">
                <div className={styles.titleBox}>
                  <p className={styles.titleAlt}>{t("login.questions")}</p>
                  <h3>{t("titles.questionstitle")}</h3>
                  <div className={styles.border} />
                </div>
              </div>
            </div>

            <div className="row" style={{ marginTop: "30px" }}>
              <div className="col-md-5 col-md-offset-1">
                <div className={styles.questionBox}>{t("login.q")}</div>
                <div id="FAQ1">
                  <h4 className={styles.question} data-wow-delay=".1s">
                    {t("login.question1")}
                  </h4>
                  <p className={styles.answer}>{t("login.answer1")}</p>
                </div>
                <div className={styles.questionBox}>{t("login.q")}</div>
                <div id="FAQ2">
                  <h4 className={styles.question}>{t("login.question2")}</h4>
                  <p className={styles.answer}>{t("login.answer2")}</p>
                </div>
                <div className={styles.questionBox}>{t("login.q")}</div>
                <div id="FAQ3">
                  <h4 className={styles.question}>{t("login.question3")}</h4>
                  <p className={styles.answer}>{t("login.answer3")}</p>
                </div>
              </div>

              <div className="col-md-5">
                <div className={styles.questionBox}>{t("login.q")}</div>
                <div id="FAQ4">
                  <h4 className={styles.question}>{t("login.question4")}</h4>
                  <p className={styles.answer}>{t("login.answer4")}</p>
                </div>

                <div className={styles.questionBox}>{t("login.q")}</div>
                <div id="FAQ5">
                  <h4 className={styles.question}>{t("login.question5")}</h4>
                  <p className={styles.answer}>{t("login.answer5")}</p>
                </div>

                <div className={styles.questionBox}>{t("login.q")}</div>
                <div id="FAQ6">
                  <h4 className={styles.question}>{t("login.question6")}</h4>
                  <p className={styles.answer}>{t("login.answer6")}</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div className={styles.footer}>
          <div className="container">
            <div className="row">
              <div className="col-md-3 col-sm-6">
                <h5>{t("login.adriaBankDirect")}</h5>
                <ul className="list-unstyled">
                  <li>
                    <a href="">{t("titles.home")}</a>
                  </li>
                  <li>
                    <a href="">{t("login.feature")}</a>
                  </li>
                  <li>
                    <a href="">{t("login.FAQ")}</a>
                  </li>
                </ul>
              </div>

              <div className="col-md-3 col-sm-6">
                <h5>{t("login.social")}</h5>
                <ul className="list-unstyled">
                  <li>
                    <a href="">{t("login.fb")}</a>
                  </li>
                  <li>
                    <a href="">{t("login.twitter")}</a>
                  </li>
                  <li>
                    <a href="">{t("login.lnk")}</a>
                  </li>
                </ul>
              </div>

              <div className="col-md-3 col-sm-6">
                <h5>{t("login.support")}</h5>
                <ul className="list-unstyled">
                  <li>
                    <a href="">{t("login.help")}</a>
                  </li>
                  <li>
                    <a href="">{t("login.privPolicy")}</a>
                  </li>
                  <li>
                    <a href="">{t("login.terms")}</a>
                  </li>
                </ul>
              </div>

              <div className="col-md-3 col-sm-6">
                <h5>{t("login.contact")}</h5>
                <p>
                  {t("login.address1")} <br /> {t("login.address2")} <br />{" "}
                  {t("login.phone")} <br />
                  {t("login.email")}{" "}
                </p>
              </div>
            </div>

            <div className="row">
              <div className="col-sm-12">
                <div className={styles.footerAlt}>
                  <p className="pull-right" style={{ lineHeight: "40px" }}>
                    2019 © 00000 Business & Technology
                  </p>
                  <a className="logo" href="#">
                    <img
                      src={require("../../../themes/00000/images/login/logoFooter.png")}
                      style={{ opacity: ".7", height: "40px" }}
                    />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// Redux form controller
LoginComponent = reduxForm(
  {
    form: "loginForm",
    fields: ["username", "password", "codeBanque", "langue"],
    validate,
    destroyOnUnmount: true,
  },
  (state) =>
    state.loginReducer.codeBanque
      ? {
          initialValues: {
            codeBanque: state.loginReducer.codeBanque,
            langue: state.loginReducer.codeLangue,
          },
        }
      : {
          initialValues: {
            codeBanque: "00000",
            langue: "fr",
          },
        }
)(LoginComponent);

// Export the UI
export default withNamespaces(["messages"])(LoginComponent);
