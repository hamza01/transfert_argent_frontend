import React, {Component} from 'react';
import {
    Button,
    Col,
    FormControl,
    Link,
    ControlLabel,
    Modal,
    responsive,
    Row,
    Tooltip,
    OverlayTrigger,
    ButtonGroup
} from 'react-bootstrap';
import {initializeWithKey, reduxForm, change} from 'redux-form';
import {browserHistory} from 'react-router';
import {translate} from 'react-i18next';
import Griddle from 'griddle-react';
import * as CashActions from '../Cashing/CashingReducer';
import * as HistoriqueActions from '../Historique/HistoriqueReducer';
import {connect} from 'react-redux';
import HistoriquePagination from './ViderCompteHistoriquePagination';
import {asyncConnect} from "redux-async-connect";
import { reset } from 'redux-form';
import DatePicker from 'react-datepicker';


class HeaderComponent extends Component {
    render() {
        return (<div
            style={{fontSize: '12px', fontWeight: 'bold'}}
        >{this.props.displayName}</div>);
    }
}

class ActionHeaderComponent extends Component {
    render() {
        return (<div className="text-center" style={{fontSize: '12px', fontWeight: 'bold', width:'110px'}}>{this.props.displayName}</div>);
    }
}

class CenterComponent extends Component {
    render() {
        const url = this.props.data;

        if(url ==='Agent'){
            return <div>A distance</div>;
        }else if(url ==='Payrolls') {
            return <div>Payroll</div>;
        }else
            return <div>{this.props.data}</div>;
    }
}

@translate(['common'])
class NoDataComponent extends Component {
    render() {
        const {t} = this.props;
        return (
            <div className="noDataResult">
                <h4>{t('errors.noResult')}</h4>
            </div>
        );
    }
}

@connect(state => ({
    userFrontDetails: state.user.userFrontDetails,
}), {...CashActions, initializeWithKey})
class ActionComponant extends Component {
    render() {
        const {globalParams, rowData} = this.props;

        const ttipSelect = (
          <Tooltip id="tooltip" className='ttip'>consulter </Tooltip>
        );
        const ttipDelete = (
          <Tooltip id="tooltip" className='ttip'>Supprimer</Tooltip>
        );
       /* const ttipSigner = (
            <Tooltip id="tooltip" className='ttip'>signer</Tooltip>
        );*/
        return (
            <div>

                <OverlayTrigger placement="top" overlay={ttipSelect}>
                    <Button  className= 'iconbtn' 
                        onClick={() => browserHistory.push({
                            pathname: baseUrl + 'app/' + rowData.idForTnr
                        })}
                     >
                        <i className="fa fa-briefcase "/>
                    </Button>
                </OverlayTrigger>
               {/* <OverlayTrigger placement="top" overlay={ttipSigner}>
                    <Button  className= 'iconbtn'
                             onClick={() =>{this.props.AlertState("signer");this.props.signerMessage(rowData.id);this.props.loadCreDocImportToComplete('','','','','','','','','','');}}
                    >
                        <i className="fa fa-check "/>
                    </Button>
                </OverlayTrigger>*/}
                <OverlayTrigger placement="top" overlay={ttipDelete}>
                    <Button  className= 'iconbtn' 
                        onClick={(e) => {this.props.deleteTransaction(rowData.id);this.props.loadCreDocImportToComplete('','','','','','','','','',''); }}
                     >
                        <i className="fa fa-trash "/>
                    </Button>
                </OverlayTrigger>
                
            </div>
        );
    }
}



export const validate = (values,props) => {
    const errors = {};

    if (!/^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.gsm) && values.gsm !== '') {
        errors.gsm = props.t('gsm.msgErr') ;
    }

    return errors;
};


@asyncConnect([{
    deferred: false,
    promise: ({params, location, store: {dispatch, getState}}) => {
        dispatch(reset('ViderCompteSearchForm'));
    }
}])

@reduxForm({
        form: 'ViderCompteSearchForm',
        fields: ['reference', 'gsm', 'statut','dateDebut','dateFin','typeReport'],
        validate,
        initialValues: {
            reference: '',
            gsm: '',
            statut: '',
            dateDebut: '',
            dateFin: '',
            typeReport:''
        },
        destroyOnUnmount: true
    },
)

@connect(state => ({
    statuts : state.CashingReducer.statuts,
    searchLoading : state.HistoriqueReducer.searchLoading,
    search : state.HistoriqueReducer.search,
    userFrontDetails: state.user.userFrontDetails,

}), { ...CashActions, ...HistoriqueActions, initializeWithKey })

@translate(['historique'])
export default class HistoriqueViderCompte extends Component {

    constructor() {
        super();
        this.handleSearch = this.handleSearch.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleExport = this.handleExport.bind(this);
        this.handleChangeTypeReport = this.handleChangeTypeReport.bind(this);
        this.handleChangeDateDebut=this.handleChangeDateDebut.bind(this);
        this.handleChangeDateFin=this.handleChangeDateFin.bind(this);
        this.state = {
            showPanelSearch: true,
            dateSaisie:'',
            dateDebut: '',
            dateFin: '',
            selectTypeReport:false,

        };

    }

    componentWillReceiveProps(nextProps) {
        if(this.props.search !== nextProps.search && nextProps.search === ""){
            this.props.dispatch(reset('ViderCompteSearchForm'));
        }
    }

    handleChange(event){
        this.props.dispatch(change('ViderCompteSearchForm', 'statut', event.target.value));
    }
    handleChangeTypeReport(event){
        this.props.dispatch(change('ViderCompteSearchForm', 'typeReport', event.target.value));
    }

    handleReset() {
        this.props.loadViderCompte(1, '');
        this.props.dispatch(reset('ViderCompteSearchForm'));
        this.setState({
            dateDebut: '',
            dateFin: ''
        })
    }

    handleChangeDateDebut = (date) => {
        this.setState({
            dateDebut: date
        })
    };

    handleChangeDateFin = (date) => {
        this.setState({
            dateFin: date
        })
    };

    handleSearch(values) {
        let search = '';

        if (/^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.gsm) && values.gsm !== '') {
            search = search + "beneficiaire:" + values.gsm.substring(values.gsm.length-9, values.gsm.length);
        }

        if (values.statut !== '' && values.statut !== 'Choisir un statut') {
            if(search !== '')
                search = search + ',';
            search = search + "statut:" + values.statut;
        }

        if(values.reference !== ''){
            if(search !== '')
                search = search + ',';
            search = search + "identifiantDemande:" + values.reference;
        }

        if(values.dateDebut !== ''){
            if(search !== ''){search += ","}
            search = search + "createdOn>" + values.dateDebut.replace(/\//gi, '_');
        }

        if(values.dateFin !== ''){
            if(search !== ''){search += ","}
            search = search + "createdOn<" + values.dateFin.replace(/\//gi, '_');
        }

        this.props.loadViderCompte(1, search);
    }

    handleExport(values) {
        let search = '';
        let index = 1;

        if (/^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.gsm) && values.gsm !== '') {
            search = search + "beneficiaire:" + values.gsm.substring(values.gsm.length-9, values.gsm.length);
        }

        if (values.statut !== '' && values.statut !== 'Choisir un statut') {
            if(search !== '')
                search = search + ',';
            search = search + "statut:" + values.statut;
        }

        if(values.reference !== ''){
            if(search !== '')
                search = search + ',';
            search = search + "identifiantDemande:" + values.reference;
        }

        if(values.dateDebut !== ''){
            if(search !== ''){search += ","}
            search = search + "createdOn>" + values.dateDebut.replace(/\//gi, '_');
        }
        if(values.dateFin !== ''){
            if(search !== ''){search += ","}
            search = search + "createdOn<" + values.dateFin.replace(/\//gi, '_');
        }

        this.setState({selectTypeReport:false});

        window.open(baseUrl + 'cash/ExportCash?search=sens:UL,' + search + '&dateDebut='+ values.dateDebut +
            '&dateFin='+ values.dateFin  + '&sens=UL'  +'&typeReport=' + values.typeReport + '&page=' + index + '&offset=0&max=10');

    }
    showExportModal(){
        this.setState({selectTypeReport:true});
    }

    render() {
        const {t, userFrontDetails,listViderCompte, handleSubmit, searchLoading, values, statuts, fields: {gsm, reference, statut, dateDebut, dateFin,typeReport}} = this.props;
        const showCommission = userFrontDetails.roles.indexOf('ROLE_EDP_COMMISSIONNEMENT') !== -1;

        const closeClotureModal = () => {
            this.setState({
                selectTypeReport: false,
            });
        };
        const styles = require('../Souscription/Souscription.scss');
        return (
            <div>
                <Modal show={searchLoading} className="loadingModal" backdrop="static" keyboard={false}>
                    <Modal.Body>
                        <Row>
                            <Col xs={12} md={12}>
                                <div className="spinner">
                                    <span style={{fontSize: '11px'}}>{t('chargement.title')}</span>
                                </div>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                <Modal
                    show={this.state.selectTypeReport}
                    onHide={closeClotureModal}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">
                            <div>{t('form.label.typeDoc')}</div>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div>

                            <Row className={styles.fieldRow}>
                                <Col xs="4" md="6">
                                    <ControlLabel>{t('form.message.selectTypeReport')}</ControlLabel>

                                    <FormControl componentClass="select" {...typeReport}
                                                 ref="typeReport"
                                                 onChange={this.handleChangeTypeReport}
                                                 className={styles.datePickerFormControl}>
                                        <option value='excel'>Excel</option>
                                        <option value='pdf' >Pdf</option>

                                    </FormControl>

                                </Col>


                            </Row>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <ButtonGroup className="pull-right" bsSize="small">
                            <Button  bsStyle="primary" style={{ marginTop: '8%' }}
                                     onClick={() => {
                                         this.handleExport(values);
                                     }}>
                                <i className="fa fa-refresh"/> {t('form.buttons.exporter')} </Button>

                        </ButtonGroup>
                    </Modal.Footer>
                </Modal>
                <div>
                    <Row>
                        <form className="formContainer" style={{marginTop: '20px', paddingBottom : '20px'}}>
                            <Row className={styles.fieldRow}>
                                <Col xs="12" md="2">
                                    <ControlLabel>{t('form.label.phone')}</ControlLabel>

                                    <FormControl type="text" {...gsm}
                                                 className={styles.datePickerFormControl}/>
                                    {gsm.error && gsm.touched &&
                                    <div className={styles.error}>
                                        <i className="fa fa-exclamation-triangle"
                                           aria-hidden="true">{gsm.error}</i>
                                    </div>
                                    }
                                </Col>
                                <Col xs="12" md="2">
                                    <ControlLabel>{t('form.label.statut')}</ControlLabel>
                                    <FormControl componentClass="select" {...statut}
                                                 ref="typeCodeList"
                                                 onChange={this.handleChange}
                                                 className={styles.datePickerFormControl}>
                                        <option hidden>{t('form.label.choisirStatut')}</option>
                                        {
                                            statuts && statuts.length && statuts.map((typeP) =>
                                                <option
                                                    key={typeP.id}
                                                    value={typeP.code}
                                                >{typeP.libelle}</option>
                                            )
                                        }

                                    </FormControl>
                                </Col>
                                <Col xs="12" md="2">
                                    <ControlLabel>{t('form.label.reference')}</ControlLabel>

                                    <FormControl type="text" {...reference}
                                                 className={styles.datePickerFormControl}/>
                                </Col>

                                <Col xs="12" md="2">
                                        <ControlLabel>{t('form.label.dateDebut')}</ControlLabel>
                                        <span {...dateDebut}>
                                                    <DatePicker
                                                        placeholderText={t('form.label.dateDebutRecherche')}
                                                        onChange={this.handleChangeDateDebut}
                                                        selected={this.state.dateDebut}
                                                        dateDebut={this.state.dateDebut}
                                                        className={styles.datePickerFormControl}
                                                        isClearable="true"
                                                        locale="fr-FR"
                                                        dateFormat="DD/MM/YYYY"
                                                    />
                                                </span>
                                    </Col>

                                <Col xs="12" md="2">
                                        <ControlLabel>{t('form.label.dateFin')}</ControlLabel>
                                        <span {...dateFin}>
                                                    <DatePicker
                                                        placeholderText={t('form.label.dateFinRecherche')}
                                                        onChange={this.handleChangeDateFin}
                                                        selected={this.state.dateFin}
                                                        dateFin={this.state.dateFin}
                                                        className={styles.datePickerFormControl}
                                                        isClearable="true"
                                                        locale="fr-FR"
                                                        dateFormat="DD/MM/YYYY"
                                                    />
                                                </span>
                                    </Col>

                                <Col xs="12" md="2">
                                    <Button style={{marginTop: ' 8%'}} loading={searchLoading} onClick={handleSubmit(() => {
                                        this.handleSearch(values);
                                    })} bsStyle="primary">
                                        <i className="fa fa-search "/> {t('form.buttons.search')}
                                    </Button>
                                    <Button style={{marginTop: ' 8%'}} loading={searchLoading} onClick={handleSubmit(() => {
                                        this.handleReset();
                                    })} bsStyle="primary">
                                        <i className="fa fa-refresh "/> {t('form.buttons.reset')}
                                    </Button>
                                    {showCommission &&
                                    <Button style={{marginTop: ' 8%'}} loading={searchLoading}
                                            onClick={handleSubmit(() => {
                                                this.showExportModal();
                                            })} bsStyle="primary">
                                        <i className="fa fa-file-pdf-o fa-4"/> {t('form.buttons.exporter')}
                                    </Button>
                                    }
                                </Col>
                            </Row>
                        </form>
                    </Row>
                    {showCommission &&
                    <div className="table-responsive tablediv">

                        <Griddle
                            results={listViderCompte}
                            tableClassName="table"
                            customNoDataComponent={NoDataComponent}
                            useGriddleStyles={false}
                            useCustomPagerComponent="true"
                            resultsPerPage={10}
                            customPagerComponent={HistoriquePagination}
                            columns={['dateCreation', 'identifiantDemande', 'agent', 'beneficiaire', 'motif', 'montantFormatted', 'commission', 'statut']}
                            columnMetadata={
                                [
                                    {
                                        columnName: 'dateCreation',
                                        displayName: t('list.cols.dateCreation'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: false
                                    },
                                    {
                                        columnName: 'identifiantDemande',
                                        displayName: t('list.cols.reference'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: false
                                    },
                                    {
                                        columnName: 'agent',
                                        displayName: t('list.cols.agent'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: false
                                    },
                                    {
                                        columnName: 'beneficiaire',
                                        displayName: t('list.cols.beneficiaire'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: true
                                    },
                                    {
                                        columnName: 'motif',
                                        displayName: t('list.cols.motif'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: true

                                    },
                                    {
                                        columnName: 'montantFormatted',
                                        displayName: t('list.cols.montant'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: true

                                    },
                                    {
                                        columnName: 'commission',
                                        displayName: t('list.cols.commission'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: true

                                    },
                                    {
                                        columnName: 'statut',
                                        displayName: t('list.cols.statut'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: true

                                    }
                                ]}
                        />
                    </div>
                    }
                    {!showCommission &&
                    <div className="table-responsive tablediv">

                        <Griddle
                            results={listViderCompte}
                            tableClassName="table"
                            customNoDataComponent={NoDataComponent}
                            useGriddleStyles={false}
                            useCustomPagerComponent="true"
                            resultsPerPage={10}
                            customPagerComponent={HistoriquePagination}
                            columns={['dateCreation', 'identifiantDemande', 'agent', 'beneficiaire', 'motif', 'montantFormatted', 'statut']}
                            columnMetadata={
                                [
                                    {
                                        columnName: 'dateCreation',
                                        displayName: t('list.cols.dateCreation'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: false
                                    },
                                    {
                                        columnName: 'identifiantDemande',
                                        displayName: t('list.cols.reference'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: false
                                    },
                                    {
                                        columnName: 'agent',
                                        displayName: t('list.cols.agent'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: false
                                    },
                                    {
                                        columnName: 'beneficiaire',
                                        displayName: t('list.cols.beneficiaire'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: true
                                    },
                                    {
                                        columnName: 'motif',
                                        displayName: t('list.cols.motif'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: true

                                    },
                                    {
                                        columnName: 'montantFormatted',
                                        displayName: t('list.cols.montant'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: true

                                    },
                                    {
                                        columnName: 'statut',
                                        displayName: t('list.cols.statut'),
                                        customHeaderComponent: HeaderComponent,
                                        customComponent: CenterComponent,
                                        sortable: true

                                    }
                                ]}
                        />
                    </div>
                }

                </div>
            </div>
        );

    }
}
