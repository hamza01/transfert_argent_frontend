const LOAD = 'historique/LOAD_REQUEST';
const LOAD_SUCCESS = 'historique/LOAD_SUCCESSFUL';
const LOAD_FAIL = 'historique/LOAD_FAILURE';
/*------*/

const LOAD_BEN = 'historique/LOAD_BEN';
const LOAD_BEN_SUCCESS = 'historique/LOAD_BEN_SUCCESS';
const LOAD_BEN_FAIL = 'historique/LOAD_BEN_FAIL';

const LOAD_TRANSFERT_STATUS = 'historique/LOAD_TRANSFERT_STATUS_REQUEST';
const LOAD_TRANSFERT_STATUS_SUCCESS = 'historique/LOAD_TRANSFERT_STATUS_SUCCESSFUL';
const LOAD_TRANSFERT_STATUS_FAIL = 'historique/LOAD_TRANSFERT_STATUS_FAILURE';

const LOAD_SOUSCRIPTION_STATUS = 'historique/LOAD_SOUSCRIPTION_STATUS_REQUEST';
const LOAD_SOUSCRIPTION_STATUS_SUCCESS = 'historique/LOAD_SOUSCRIPTION_STATUS_SUCCESSFUL';
const LOAD_SOUSCRIPTION_STATUS_FAIL = 'historique/LOAD_SOUSCRIPTION_STATUS_FAILURE';

const SET_PAGE_TITLE = 'historique/SET_PAGE_TITLE';
const LOAD_HISTORIQUE_SOUSCRIPTION = 'historique/LOAD_HISTORIQUE_SOUSCRIPTION';
const LOAD_HISTORIQUE_SOUSCRIPTION_SUCCESS = 'historique/LOAD_HISTORIQUE_SOUSCRIPTION_SUCCESS';
const LOAD_HISTORIQUE_SOUSCRIPTION_FAIL = 'historique/LOAD_HISTORIQUE_SOUSCRIPTION_FAIL';

const LOAD_HISTORIQUE_TRANSFERT = 'historique/LOAD_HISTORIQUE_TRANSFERT';
const LOAD_HISTORIQUE_TRANSFERT_SUCCESS = 'historique/LOAD_HISTORIQUE_TRANSFERT_SUCCESS';
const LOAD_HISTORIQUE_TRANSFERT_FAIL = 'historique/LOAD_HISTORIQUE_TRANSFERT_FAIL';

const LOAD_HISTORIQUE_PAYMENT = 'historique/LOAD_HISTORIQUE_PAYMENT';
const LOAD_HISTORIQUE_PAYMENT_SUCCESS = 'historique/LOAD_HISTORIQUE_PAYMENT_SUCCESS';
const LOAD_HISTORIQUE_PAYMENT_FAIL = 'historique/LOAD_HISTORIQUE_PAYMENT_FAIL';

const LOAD_HISTORIQUE_CASH_IN = 'historique/LOAD_HISTORIQUE_CASH_IN';
const LOAD_HISTORIQUE_CASH_IN_SUCCESS = 'historique/LOAD_HISTORIQUE_CASH_IN_SUCCESS';
const LOAD_HISTORIQUE_CASH_IN_FAIL = 'historique/LOAD_HISTORIQUE_CASH_IN_FAIL';

const LOAD_HISTORIQUE_VIREMENT = 'historique/LOAD_HISTORIQUE_VIREMENT';
const LOAD_HISTORIQUE_VIREMENT_SUCCESS = 'historique/LOAD_HISTORIQUE_VIREMENT_SUCCESS';
const LOAD_HISTORIQUE_VIREMENT_FAIL = 'historique/LOAD_HISTORIQUE_VIREMENT_FAIL';

const LOAD_HISTORIQUE_BENEFICIAIRE = 'historique/LOAD_HISTORIQUE_BENEFICIAIRE';
const LOAD_HISTORIQUE_BENEFICIAIRE_SUCCESS = 'historique/LOAD_HISTORIQUE_BENEFICIAIRE_SUCCESS';
const LOAD_HISTORIQUE_BENEFICIAIRE_FAIL = 'historique/LOAD_HISTORIQUE_BENEFICIAIRE_FAIL';

const LOAD_HISTORIQUE_CASH_OUT = 'historique/LOAD_HISTORIQUE_CASH_OUT';
const LOAD_HISTORIQUE_CASH_OUT_SUCCESS = 'historique/LOAD_HISTORIQUE_CASH_OUT_SUCCESS';
const LOAD_HISTORIQUE_CASH_OUT_FAIL = 'historique/LOAD_HISTORIQUE_CASH_OUT_FAIL';

const LOAD_HISTORIQUE_VIDER_COMPTE = 'historique/LOAD_HISTORIQUE_VIDER_COMPTE';
const LOAD_HISTORIQUE_VIDER_COMPTE_SUCCESS = 'historique/LOAD_HISTORIQUE_VIDER_COMPTE_SUCCESS';
const LOAD_HISTORIQUE_VIDER_COMPTE_FAIL = 'historique/LOAD_HISTORIQUE_VIDER_COMPTE_FAIL';

const LOAD_HISTORIQUE_CASH_IN_AGENT = 'historique/LOAD_HISTORIQUE_CASH_IN_AGENT';
const LOAD_HISTORIQUE_CASH_IN_AGENT_SUCCESS = 'historique/LOAD_HISTORIQUE_CASH_IN_AGENT_SUCCESS';
const LOAD_HISTORIQUE_CASH_IN_AGENT_FAIL = 'historique/LOAD_HISTORIQUE_CASH_IN_AGENT_FAIL';

const LOAD_HISTORIQUE_CASH_OUT_AGENT = 'historique/LOAD_HISTORIQUE_CASH_OUT_AGENT';
const LOAD_HISTORIQUE_CASH_OUT_AGENT_SUCCESS = 'historique/LOAD_HISTORIQUE_CASH_OUT_AGENT_SUCCESS';
const LOAD_HISTORIQUE_CASH_OUT_AGENT_FAIL = 'historique/LOAD_HISTORIQUE_CASH_OUT_AGENT_FAIL';

const LOAD_HISTORIQUE_UPGRADE = 'historique/LOAD_HISTORIQUE_UPGRADE';
const LOAD_HISTORIQUE_UPGRADE_SUCCESS = 'historique/LOAD_HISTORIQUE_UPGRADE_SUCCESS';
const LOAD_HISTORIQUE_UPGRADE_FAIL = 'historique/LOAD_HISTORIQUE_UPGRADE_FAIL';

const UPGRADE_INSTANCE = 'historique/UPGRADE_INSTANCE';
const UPGRADE_INSTANCE_SUCCESS = 'historique/UPGRADE_INSTANCE_SUCCESS';
const UPGRADE_INSTANCE_FAIL = 'historique/UPGRADE_INSTANCE_FAIL';

const LOAD_HISTORIQUE_RECLAMATION = 'historique/LOAD_HISTORIQUE_RECLAMATION';
const LOAD_HISTORIQUE_RECLAMATION_SUCCESS = 'historique/LOAD_HISTORIQUE_RECLAMATION_SUCCESS';
const LOAD_HISTORIQUE_RECLAMATION_FAIL = 'historique/LOAD_HISTORIQUE_RECLAMATION_FAIL';

const LOAD_HISTORIQUE_RECLAMATION_AGENT = 'historique/LOAD_HISTORIQUE_RECLAMATION';
const LOAD_HISTORIQUE_RECLAMATION_AGENT_SUCCESS = 'historique/LOAD_HISTORIQUE_RECLAMATION_AGENT_SUCCESS';
const LOAD_HISTORIQUE_RECLAMATION_AGENT_FAIL = 'historique/LOAD_HISTORIQUE_RECLAMATION_AGENT_FAIL';


const LOAD_RECLAMATION_STATUS = 'historique/LOAD_RECLAMATION_STATUS_REQUEST';
const LOAD_RECLAMATION_STATUS_SUCCESS = 'historique/LOAD_RECLAMATION_STATUS_SUCCESSFUL';
const LOAD_RECLAMATION_STATUS_FAIL = 'historique/LOAD_RECLAMATION_STATUS_FAILURE';

const LOAD_RECLAMATION_AGENT_STATUS = 'historique/LOAD_RECLAMATION_AGENT_STATUS_REQUEST';
const LOAD_RECLAMATION_AGENT_STATUS_SUCCESS = 'historique/LOAD_RECLAMATION_AGENT_STATUS_SUCCESSFUL';
const LOAD_RECLAMATION_AGENT_STATUS_FAIL = 'historique/LOAD_RECLAMATION_AGENT_STATUS_FAILURE';

const LOAD_HISTORIQUE_TRANSACTIONS = 'historique/LOAD_HISTORIQUE_TRANSACTIONS';
const LOAD_HISTORIQUE_TRANSACTIONS_SUCCESS = 'historique/LOAD_HISTORIQUE_TRANSACTIONS_SUCCESS';
const LOAD_HISTORIQUE_TRANSACTIONS_FAIL = 'historique/LOAD_HISTORIQUE_TRANSACTIONS_FAIL';

const LOAD_OPERATION_TYPES = 'historique/LOAD_OPERATION_TYPES';
const LOAD_OPERATION_TYPES_SUCCESS = 'historique/LOAD_OPERATION_TYPES_SUCCESS';
const LOAD_OPERATION_TYPES_FAIL = 'historique/LOAD_OPERATION_TYPES_FAIL';

const SET_TAB_INDEX = 'historique/SET_TAB_INDEX';


const RESET_ALERT = 'CredocExport/RESET_ALERT';

const initialState = {};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case LOAD:
            return {
                ...state,
                loading: true
            };
        case LOAD_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                loadedData: action.result,
                error: null,
            };
        case LOAD_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                loadedData: null,
                error: action.error
            };

        /*------------------------------------------------------------------------------------------*/
        case SET_PAGE_TITLE:
            return {
                ...state,
                title: action.title
            };
        case LOAD_SOUSCRIPTION_STATUS:
            return {
                ...state,
                loading: true
            };
        case LOAD_SOUSCRIPTION_STATUS_SUCCESS: {
            return {
                ...state,
                loading: false,
                loaded: true,
                souscriptionStatuts : action.result.souscriptionStatutsList
            };
        }
        case LOAD_SOUSCRIPTION_STATUS_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true
            };

        case LOAD_RECLAMATION_STATUS:
            return {
                ...state,
                loading: true
            };
        case LOAD_RECLAMATION_STATUS_SUCCESS: {
            return {
                ...state,
                loading: false,
                loaded: true,
                reclamationStatuts : action.result.reclamationStatutsList
            };
        }
        case LOAD_RECLAMATION_STATUS_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true
            };

        case LOAD_RECLAMATION_AGENT_STATUS:
            return {
                ...state,
                loading: true
            };
        case LOAD_RECLAMATION_AGENT_STATUS_SUCCESS: {
            return {
                ...state,
                loading: false,
                loaded: true,
                reclamationAgentStatuts : action.result.reclamationStatutsList
            };
        }
        case LOAD_RECLAMATION_AGENT_STATUS_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true
            };

        case LOAD_TRANSFERT_STATUS:
            return {
                ...state,
                loading: true
            };
        case LOAD_TRANSFERT_STATUS_SUCCESS: {
            return {
                ...state,
                loading: false,
                loaded: true,
                tp2pStatuts : action.result.tp2pList
            };
        }
        case LOAD_TRANSFERT_STATUS_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true
            };
        case LOAD_HISTORIQUE_TRANSFERT:
            return {
                ...state,
                loadingHistorique: true
            };
        case LOAD_HISTORIQUE_TRANSFERT_SUCCESS: {
            let list = action.result.demandeList;
            list.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                searchT : action.search,
                typeOperationT : action.typeOperation,
                index : action.index,
                loadingHistorique: false,
                loaded: true,
                listTransfert: list,
                transfertMaxPages: action.result.total
            };
        }
        case LOAD_HISTORIQUE_TRANSFERT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                loadingHistorique: false
            };
        case LOAD_HISTORIQUE_PAYMENT:
            return {
                ...state,
                loadingHistorique: true
            };
        case LOAD_HISTORIQUE_PAYMENT_SUCCESS: {
            let list = action.result.demandeList;
            list.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                searchP : action.search,
                index : action.index,
                typeOperationP : action.typeOperation,
                loadingHistorique: false,
                loaded: true,
                listPayment: list,
                paimentMaxPages: action.result.total
            };
        }
        case LOAD_HISTORIQUE_PAYMENT_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true,
                loadingHistorique: false
            };

        case LOAD_HISTORIQUE_BENEFICIAIRE:
            return {
                ...state,
                beneficiaireSearchLoading: true
            };
        case LOAD_HISTORIQUE_BENEFICIAIRE_SUCCESS: {
            let list = action.result.bensList;
            list.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                search : action.search,
                index : action.index,
                beneficiaireSearchLoading: false,
                loaded: true,
                listBeneficiaire: list,
                beneficiaireMaxPages: action.result.total
            };
        }
        case LOAD_HISTORIQUE_BENEFICIAIRE_FAIL:
            return {
                ...state,
                searchLoading: false,
                beneficiaireSearchLoading: false,
                loaded: true
            };

        case LOAD_HISTORIQUE_VIREMENT:
            return {
                ...state,
                virementSearchLoading: true
            };
        case LOAD_HISTORIQUE_VIREMENT_SUCCESS: {
            let list = action.result.demandeList;
            list.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                search : action.search,
                index : action.index,
                virementSearchLoading: false,
                loaded: true,
                listVirement: list,
                virementMaxPages: action.result.total
            };
        }
        case LOAD_HISTORIQUE_VIREMENT_FAIL:
            return {
                ...state,
                searchLoading: false,
                virementSearchLoading: false,
                loaded: true
            };
        case LOAD_BEN:
            return {
                ...state,
                searchLoading: true,
            };
        case LOAD_BEN_SUCCESS:
            if(action.result.success) {
                return {
                    ...state,
                    searchLoading: false,
                    beneficiaires: action.result.beneficiaires
                };
            }else{
                return {
                    ...state,
                    searchLoading: false,
                }
            }
        case LOAD_BEN_FAIL:
            return {
                ...state,
                searchLoading: false,
            };
        case LOAD_HISTORIQUE_SOUSCRIPTION:
            return {
                ...state,
                souscriptionSearchLoading: true
            };
        case LOAD_HISTORIQUE_SOUSCRIPTION_SUCCESS: {
            let list = action.result.demandeList;
            list.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                search : action.search,
                index : action.index,
                souscriptionSearchLoading: false,
                loaded: true,
                listSouscription: list,
                souscriptionMaxPages: action.result.total
            };
        }
        case LOAD_HISTORIQUE_SOUSCRIPTION_FAIL:
            return {
                ...state,
                searchLoading: false,
                loaded: true
            };

        case LOAD_HISTORIQUE_CASH_IN:
            return {
                ...state,
                searchLoading: true
            };
        case LOAD_HISTORIQUE_CASH_IN_SUCCESS: {
            let list = action.result.list;
            return {
                ...state,
                searchCashIn : action.search,
                index : action.index,
                listCashIn: list,
                cashInMaxPages: action.result.total,
                searchLoading: false,
                loaded: true
            };
        }
        case LOAD_HISTORIQUE_CASH_IN_FAIL:
            return {
                ...state,
                loadingHistorique: false,
                loaded: true
            };


        case LOAD_HISTORIQUE_CASH_OUT:
            return {
                ...state,
                searchLoading: true
            };
        case LOAD_HISTORIQUE_CASH_OUT_SUCCESS: {
            let list = action.result.list;
            return {
                ...state,
                searchCashOut : action.search,
                index : action.index,
                listCashOut: list,
                cashOutMaxPages: action.result.total,
                searchLoading: false,
                loaded: true
            };
        }
        case LOAD_HISTORIQUE_CASH_OUT_FAIL:
            return {
                ...state,
                loadingHistorique: false,
                loaded: true
            };

        case LOAD_HISTORIQUE_VIDER_COMPTE:
            return {
                ...state,
                searchLoading: true
            };
        case LOAD_HISTORIQUE_VIDER_COMPTE_SUCCESS: {
            let list = action.result.list;
            return {
                ...state,
                searchViderCompte : action.search,
                index : action.index,
                listViderCompte: list,
                viderCompteMaxPages: action.result.total,
                searchLoading: false,
                loaded: true
            };
        }
        case LOAD_HISTORIQUE_VIDER_COMPTE_FAIL:
            return {
                ...state,
                loadingHistorique: false,
                loaded: true
            };

        case LOAD_HISTORIQUE_CASH_OUT_AGENT:
            return {
                ...state,
                searchLoading: true
            };
        case LOAD_HISTORIQUE_CASH_OUT_AGENT_SUCCESS: {
            let list = action.result.list;
            return {
                ...state,
                search : action.search,
                index : action.index,
                listCashOutAgent: list,
                cashOutAgentMaxPages: action.result.total,
                searchLoading: false,
                loaded: true
            };
        }
        case LOAD_HISTORIQUE_CASH_OUT_AGENT_FAIL:
            return {
                ...state,
                loadingHistorique: false,
                loaded: true
            };

        case LOAD_HISTORIQUE_CASH_IN_AGENT:
            return {
                ...state,
                searchLoading: true
            };
        case LOAD_HISTORIQUE_CASH_IN_AGENT_SUCCESS: {
            let list = action.result.list;
            return {
                ...state,
                search : action.search,
                index : action.index,
                listCashInAgent: list,
                cashINAgentMaxPages: action.result.total,
                searchLoading: false,
                loaded: true
            };
        }
        case LOAD_HISTORIQUE_CASH_IN_AGENT_FAIL:
            return {
                ...state,
                loadingHistorique: false,
                loaded: true
            };

        case LOAD_HISTORIQUE_RECLAMATION:
            return {
                ...state,
                reclamationsearchLoading: true
            };
        case LOAD_HISTORIQUE_RECLAMATION_SUCCESS: {
            let list = action.result.list;
            list.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                search : action.search,
                index : action.index,
                listReclamation: list,
                reclamationMaxPages: action.result.total,
                reclamationsearchLoading: false,
                loaded: true
            };
        }
        case LOAD_HISTORIQUE_RECLAMATION_FAIL:
            return {
                ...state,
                searchLoading: false,
                loaded: true
            };

        case LOAD_HISTORIQUE_RECLAMATION_AGENT:
            return {
                ...state,
                reclamationsearchLoading: true
            };
        case LOAD_HISTORIQUE_RECLAMATION_AGENT_SUCCESS: {
            let list = action.result.list;
            list.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                search : action.search,
                index : action.index,
                listReclamationAgent: list,
                reclamationMaxPagesAgent: action.result.total,
                reclamationsearchLoading: false,
                loaded: true
            };
        }
        case LOAD_HISTORIQUE_RECLAMATION_AGENT_FAIL:
            return {
                ...state,
                searchLoading: false,
                loaded: true
            };

        case LOAD_HISTORIQUE_UPGRADE:
            return {
                ...state,
                searchLoading: true
            };
        case LOAD_HISTORIQUE_UPGRADE_SUCCESS: {
            let list = action.result.demandeList;
            list.forEach((element) => {
                const transfer = element;
                transfer.action = 'action';
            });
            return {
                ...state,
                listUpgrade : list,
                upgradeMaxPages: action.result.total,
                searchLoading: false,
                loaded: true,
                searchUpgrade : action.search,
                index : action.index
            };
        }
        case LOAD_HISTORIQUE_UPGRADE_FAIL:
            return {
                ...state,
                searchLoading: false,
                loaded: true
            };
        case UPGRADE_INSTANCE:
            return {
                ...state,
                loading: true
            };
        case UPGRADE_INSTANCE_SUCCESS: {
            return {
                ...state,
                data : action.result.demandeDto,
                success: action.result.success,
                loading: false,
                loaded: true
            };
        }
        case UPGRADE_INSTANCE_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true
            };
        case LOAD_HISTORIQUE_TRANSACTIONS:
            return {
                ...state,
                transactionSearchLoading: true
            };
        case LOAD_HISTORIQUE_TRANSACTIONS_SUCCESS: {
            let list = action.result.transactionList;

            return {
                ...state,
                search : action.search,
                index : action.index,
                transactionSearchLoading: false,
                loaded: true,
                listTransactions: list,
                transactionMaxPages: action.result.total
            };
        }
        case LOAD_HISTORIQUE_TRANSACTIONS_FAIL:
            return {
                ...state,
                searchLoading: false,
                transactionSearchLoading: false,
                loaded: true
            };
        case LOAD_OPERATION_TYPES:
            return {
                ...state,
                loading: true
            };
        case LOAD_OPERATION_TYPES_SUCCESS: {
            return {
                ...state,
                loading: false,
                loaded: true,
                typeOperationList : action.result.typeOperationList
            };
        }
        case LOAD_OPERATION_TYPES_FAIL:
            return {
                ...state,
                loading: false,
                loaded: true
            };
        case SET_TAB_INDEX:
            let _tabIndex = action.index ? action.index : 1;
            return {
                ...state,
                tabIndex: _tabIndex
            };
        default:
            return state;
    }
}

function objectToParams(object) {
    let str = '';
    for (const key in object) {
        if (str !== '') {
            str += '&';
        }
        str += key + '=' + encodeURIComponent(object[key]);
    }
    return str;
}

/*---- */
export function setTitle(title) {
    return {
        type: SET_PAGE_TITLE,
        title
    }
}

export function setTabIndex(index) {
    return {
        type: SET_TAB_INDEX,
        index
    }
}

// for dashboard tab
export function loadCashOut(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_CASH_OUT, LOAD_HISTORIQUE_CASH_OUT_SUCCESS, LOAD_HISTORIQUE_CASH_OUT_FAIL],
        promise: (client) => client.get('cash/listObject?search=sens:UL,' + search + '&page=' + index + '&offset=0&max=10')

    };
}

export function loadViderCompte(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_VIDER_COMPTE, LOAD_HISTORIQUE_VIDER_COMPTE_SUCCESS, LOAD_HISTORIQUE_VIDER_COMPTE_FAIL],
        promise: (client) => client.get('cash/listObject?search=sens:VC,' + search + '&page=' + index + '&offset=0&max=10')

    };
}

export function loadCashIn(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_CASH_IN, LOAD_HISTORIQUE_CASH_IN_SUCCESS, LOAD_HISTORIQUE_CASH_IN_FAIL],
        promise: (client) => client.get('cash/listObject?search=sens:N1,' + search + '&page=' + index + '&offset=0&max=10')

    };
}



export function loadCashOutAgent(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_CASH_OUT_AGENT, LOAD_HISTORIQUE_CASH_OUT_AGENT_SUCCESS, LOAD_HISTORIQUE_CASH_OUT_AGENT_FAIL],
        promise: (client) => client.get('cash/listObject?search=sens:U5,' + search + '&page=' + index + '&offset=0&max=10')

    };
}

export function loadCashInAgent(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_CASH_IN_AGENT, LOAD_HISTORIQUE_CASH_IN_AGENT_SUCCESS, LOAD_HISTORIQUE_CASH_IN_AGENT_FAIL],
        promise: (client) => client.get('cash/listObject?search=sens:N5,' + search + '&page=' + index + '&offset=0&max=10')

    };
}

export function loadSouscriptionList(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_SOUSCRIPTION, LOAD_HISTORIQUE_SOUSCRIPTION_SUCCESS, LOAD_HISTORIQUE_SOUSCRIPTION_FAIL],
        promise: (client) => client.get('demandeSouscription/list?search=' + search +'&page=' + index + '&offset=0&max=10')
    };
}

export function loadTransfertHistory(index, search,typeOperation) {
    return {
        typeOperation,
        search,
        index,
        types: [LOAD_HISTORIQUE_TRANSFERT, LOAD_HISTORIQUE_TRANSFERT_SUCCESS, LOAD_HISTORIQUE_TRANSFERT_FAIL],
        promise: (client) => client.get('clienttransfert/transfertWalletlist?search=' + search + ',operationType:' + typeOperation + '&page=' + index + '&offset=0&max=10')
    };
}

export function loadPaymentHistory(index, search,typeOperation) {
    return {
        typeOperation,
        search,
        index,
        types: [LOAD_HISTORIQUE_PAYMENT, LOAD_HISTORIQUE_PAYMENT_SUCCESS, LOAD_HISTORIQUE_PAYMENT_FAIL],
        promise: (client) => client.get('clienttransfert/transfertWalletlist?search=' + search + ',operationType:' + typeOperation + '&page=' + index + '&offset=0&max=10')
    };
}

export function loadP2pStatuts() {
    return {
        types: [LOAD_TRANSFERT_STATUS, LOAD_TRANSFERT_STATUS_SUCCESS, LOAD_TRANSFERT_STATUS_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getTP2PStatus')
    };
}

export function loadSouscriptionStatuts() {
    return {
        types: [LOAD_SOUSCRIPTION_STATUS, LOAD_SOUSCRIPTION_STATUS_SUCCESS, LOAD_SOUSCRIPTION_STATUS_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getSouscriptionStatus')
    };
}

export function loadVirementHistory(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_VIREMENT, LOAD_HISTORIQUE_VIREMENT_SUCCESS, LOAD_HISTORIQUE_VIREMENT_FAIL],
        promise: (client) => client.get('virementSIMT/list?search=' + search + '&page=' + index + '&offset=0&max=10')
    };
}

export function loadBeneficiaireHistory(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_VIREMENT, LOAD_HISTORIQUE_VIREMENT_SUCCESS, LOAD_HISTORIQUE_VIREMENT_FAIL],
        promise: (client) => client.get('virementSIMT/listerBens?search=' + search + '&page=' + index + '&offset=0&max=10')
    };
}

export function loadBens() {
    return {
        types: [LOAD_BEN, LOAD_BEN_SUCCESS, LOAD_BEN_FAIL],
        promise: (client) => client.get('virementSIMT/getBens')
    };
}

export function loadUpgradeList(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_UPGRADE, LOAD_HISTORIQUE_UPGRADE_SUCCESS, LOAD_HISTORIQUE_UPGRADE_FAIL],
        promise: (client) => client.get('api/subscriber/upgradeList?search=' + search + '&page=' + index + '&offset=0&max=10')
    };
}

export function upgradeInstance(id) {
    return {
        types: [UPGRADE_INSTANCE, UPGRADE_INSTANCE_SUCCESS, UPGRADE_INSTANCE_FAIL],
        promise: (client) => client.get('api/subscriber/upgradeInstance/' + id)
    };
}

export function loadReclamationHistory(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_RECLAMATION, LOAD_HISTORIQUE_RECLAMATION_SUCCESS, LOAD_HISTORIQUE_RECLAMATION_FAIL],
        promise: (client) => client.get('reclamation/listClient?search=' + search + ',type:client'+'&page=' + index + '&offset=0&max=10')
    };
}

export function loadReclamationAgentHistory(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_RECLAMATION_AGENT, LOAD_HISTORIQUE_RECLAMATION_AGENT_SUCCESS, LOAD_HISTORIQUE_RECLAMATION_AGENT_FAIL],
        promise: (client) => client.get('reclamation/listAgent?search=' + search + ',type:agent'+'&page=' + index + '&offset=0&max=10')
    };
}

export function loadReclamationStatuts() {
    return {
        types: [LOAD_RECLAMATION_STATUS, LOAD_RECLAMATION_STATUS_SUCCESS, LOAD_RECLAMATION_STATUS_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getReclamationStatus')
    };
}

export function loadReclamationAgentStatuts() {
    return {
        types: [LOAD_RECLAMATION_AGENT_STATUS, LOAD_RECLAMATION_AGENT_STATUS_SUCCESS, LOAD_RECLAMATION_AGENT_STATUS_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getReclamationAgentStatus')
    };
}

export function loadTransactionsList(index, search) {
    return {
        search,
        index,
        types: [LOAD_HISTORIQUE_TRANSACTIONS, LOAD_HISTORIQUE_TRANSACTIONS_SUCCESS, LOAD_HISTORIQUE_TRANSACTIONS_FAIL],
        promise: (client) => client.get('client/getTransactionsList?search=' + search +'&page=' + index + '&offset=0&max=10')
    };
}

export function loadOperationTypes() {
    return {
        types: [LOAD_OPERATION_TYPES, LOAD_OPERATION_TYPES_SUCCESS, LOAD_OPERATION_TYPES_FAIL],
        promise: (client) => client.get('nomenclatureCodification/getOperationTypes')
    };
}
