/**
 * Created by lenovo on 25/12/2018.
 */
import React, { Component, PropTypes } from 'react';
import { Button, Pagination } from 'react-bootstrap';
import { connect } from 'react-redux';
import { initializeWithKey } from 'redux-form';
import * as HistoriqueActions from '../Historique/HistoriqueReducer';

@connect(
    state => ({
        maxPages: state.HistoriqueReducer.virementMaxPages,
        search: state.HistoriqueReducer.search,
        index: state.HistoriqueReducer.index
    }),
    { ...HistoriqueActions, initializeWithKey })

export default class PaginationHistorique extends Component {
    static propTypes = {
        loadVirementHistory: PropTypes.func,
        maxPages: PropTypes.number
    };
    constructor() {
        super();
        this.state = {
            activePage: 1,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.index !== nextProps.index && this.props.index !== 1 && nextProps.index === 1) {
            this.setState({activePage : nextProps.index});
        }
    }

    render() {
        const { loadVirementHistory, maxPages, search} = this.props;
        let maxPage = 0;
        if (maxPages % 10 === 0) {
            maxPage = maxPages / 10;
        } else {
            maxPage = Math.ceil(maxPages / 10);
        }
        const changePage = (event) => {
            this.setState({
                activePage: event
            });
            if(search && search !== "") {
                loadVirementHistory(event, search);
            }else{
                loadVirementHistory(event, '');
            }
        };
        return (
            <div>
                { maxPages > 10 ? <Pagination
                    prev
                    next
                    first
                    last
                    ellipsis
                    boundaryLinks
                    items={maxPage}
                    maxButtons={5}
                    bsSize="medium"
                    activePage={this.state.activePage}
                    onSelect={changePage}
                />
                    :
                    <div />
                }
            </div>
        );
    }
}

