// import React, { Component, PropTypes } from "react";
// import { Row, Tab, Tabs, Col, Alert, Modal } from "react-bootstrap/lib";
// import { translate } from "react-i18next";

// import * as HistoriqueActions from "./HistoriqueReducer";
// import * as CashActions from "../Cashing/CashingReducer";
// import { connect } from "react-redux";
// import { initializeWithKey } from "redux-form";
// import { asyncConnect } from "redux-connect";
// import PageHeader from "react-bootstrap/lib/PageHeader";
// import HistoriqueSouscription from "./HistoriqueSouscription";
// import HistoriqueCashOut from "./HistoriqueCashOut";
// import HistoriqueCashIn from "./HistoriqueCashIn";
// import HistoriqueTransfert from "./HistoriqueTransfert";
// import HistoriquePayment from "./HistoriquePayment";
// import HistoriqueVirement from "./HistoriqueVirement";
// import * as UserActions from "../User/UserReducer";
// import HistoriqueUpgrade from "./HistoriqueUpgrade";
// import HistoriqueReclamation from "./HistoriqueReclamation";
// import { AuthorizedComponent } from "react-router-role-authorization";
// import HistoriqueReclamationAgent from "./HistoriqueReclamationAgent";
// import HistoriqueTransactions from "./HistoriqueTransactions";
// import HistoriqueViderCompte from "./HistoriqueViderCompte";

// @asyncConnect([
//   {
//     deferred: false,
//     promise: ({
//       params,
//       transfert,
//       paiement,
//       location,
//       store: { dispatch, getState },
//     }) => {
//       return Promise.all([
//         dispatch(CashActions.loadCashStatuts()),
//         dispatch(HistoriqueActions.loadP2pStatuts()),
//         dispatch(HistoriqueActions.loadBens()),
//         dispatch(HistoriqueActions.loadSouscriptionStatuts()),
//         dispatch(HistoriqueActions.loadOperationTypes()),
//         dispatch(HistoriqueActions.loadCashIn(1, "")),
//         dispatch(HistoriqueActions.loadCashOut(1, "")),
//         dispatch(HistoriqueActions.loadSouscriptionList(1, "")),
//         dispatch(
//           HistoriqueActions.loadTransfertHistory(
//             1,
//             "",
//             bankCode === "00861" ? "TRANSFER" : "W"
//           )
//         ),
//         dispatch(
//           HistoriqueActions.loadPaymentHistory(
//             1,
//             "",
//             bankCode === "00861" ? "PAYMENT" : "H"
//           )
//         ),
//         dispatch(HistoriqueActions.loadVirementHistory(1, "")),
//         dispatch(HistoriqueActions.loadUpgradeList(1, "")),
//         dispatch(HistoriqueActions.loadReclamationHistory(1, "")),
//         dispatch(HistoriqueActions.loadReclamationStatuts()),
//         dispatch(HistoriqueActions.loadReclamationAgentHistory(1, "")),
//         dispatch(HistoriqueActions.loadReclamationAgentStatuts()),
//         dispatch(HistoriqueActions.loadBeneficiaireHistory(1, "")),
//         dispatch(HistoriqueActions.loadTransactionsList(1, "")),
//         dispatch(HistoriqueActions.loadViderCompte(1, "")),
//       ]);
//     },
//   },
// ])
// @connect(
//   (state) => ({
//     loadingHistorique: state.HistoriqueReducer.loadingHistorique,
//     listSouscription: state.HistoriqueReducer.listSouscription,
//     listCashIn: state.HistoriqueReducer.listCashIn,
//     listCashOut: state.HistoriqueReducer.listCashOut,
//     listTransfert: state.HistoriqueReducer.listTransfert,
//     listVirement: state.HistoriqueReducer.listVirement,
//     listPayment: state.HistoriqueReducer.listPayment,
//     listUpgrade: state.HistoriqueReducer.listUpgrade,
//     userFrontDetails: state.user.userFrontDetails,
//     listReclamation: state.HistoriqueReducer.listReclamation,
//     listReclamationAgent: state.HistoriqueReducer.listReclamationAgent,
//     listTransactions: state.HistoriqueReducer.listTransactions,
//     listViderCompte: state.HistoriqueReducer.listViderCompte,
//   }),
//   { ...HistoriqueActions, ...UserActions, initializeWithKey }
// )
// @translate(["historique"], { wait: true })
// export default class Historique extends AuthorizedComponent {
//   constructor(props) {
//     super(props);
//     this.userRoles = this.props.userFrontDetails.roles;
//     this.notAuthorizedPath = baseUrl + "app/AccessDenied";
//   }

//   render() {
//     const {
//       t,
//       title,
//       tabIndex,
//       listSouscription,
//       listCashIn,
//       listVirement,
//       userFrontDetails,
//       listTransfert,
//       listPayment,
//       listCashOut,
//       listTransactions,
//       setTabIndex,
//       setTitle,
//       listViderCompte,
//       loadingHistorique,
//       listUpgrade,
//       listReclamation,
//       listReclamationAgent,
//     } = this.props;
//     const showMenuCashHistorique =
//       userFrontDetails.roles.indexOf("ROLE_EDP_CASH_HISTORIQUE") !== -1;
//     const showMenuTransfertHistorique =
//       userFrontDetails.roles.indexOf("ROLE_EDP_TRANSFERT_HISTORIQUE") !== -1;
//     const showVirementEDPAgent =
//       userFrontDetails.roles.indexOf("ROLE_EDP_VIREMENT_SIMT") !== -1;
//     const showUpgradeHistorique =
//       userFrontDetails.roles.indexOf("ROLE_EDP_UPGRADE_HISTORIQUE") !== -1;
//     const showReclamationHistorique =
//       userFrontDetails.roles.indexOf("ROLE_EDP_RECLAMATION_HISTORIQUE") !== -1;
//     const showCommission =
//       userFrontDetails.roles.indexOf("ROLE_EDP_COMMISSIONNEMENT") !== -1;
//     const showViderCompte =
//       userFrontDetails.roles.indexOf("ROLE_EDP_VIDER_COMPTE") !== -1;

//     const changeIndexTab = (index) => {
//       switch (index) {
//         case 1:
//           setTitle("Souscription");
//           setTabIndex(1);
//           break;
//         case 2:
//           setTitle("cash In");
//           setTabIndex(2);
//           break;
//         case 3:
//           setTitle("cash Out");
//           setTabIndex(3);
//           break;
//         case 4:
//           setTitle("Transfert");
//           setTabIndex(4);
//           break;
//         case 5:
//           setTitle("Virement");
//           setTabIndex(5);
//           break;
//         case 6:
//           setTitle("Paiement");
//           setTabIndex(6);
//           break;
//         case 8:
//           setTitle("ReclamationClient");
//           setTabIndex(8);
//           break;
//         case 9:
//           setTitle("ReclamationAgent");
//           setTabIndex(9);
//           break;
//         case 10:
//           setTitle("Transactions");
//           setTabIndex(10);
//           break;
//         // case 11:
//         //     setTitle('ViderCompte');
//         //     setTabIndex(11);
//         //     break;
//       }
//     };

//     return (
//       <div>
//         <Modal
//           show={loadingHistorique}
//           className="loadingModal"
//           backdrop="static"
//           keyboard={false}
//         >
//           <Modal.Body>
//             <Row>
//               <Col xs={12} md={12}>
//                 <div className="spinner">
//                   <span style={{ fontSize: "11px" }}>
//                     {t("chargement.title")}
//                   </span>
//                 </div>
//               </Col>
//             </Row>
//           </Modal.Body>
//         </Modal>

//         <PageHeader>
//           <h3>{t("header.title")}</h3>
//         </PageHeader>

//         <div className="formContainer" style={{ padding: "12px" }}>
//           <Tabs
//             defaultActiveKey={tabIndex}
//             onSelect={changeIndexTab}
//             className="full-width-tabs"
//           >
//             <Tab eventKey={1} title={t("header.tabTitles.souscription")}>
//               <Row>
//                 <HistoriqueSouscription
//                   listSouscription={listSouscription}
//                   t={t}
//                 />
//               </Row>
//             </Tab>
//             {showMenuCashHistorique && (
//               <Tab
//                 eventKey={2}
//                 onSelect={changeIndexTab}
//                 title={t("header.tabTitles.cashIn")}
//               >
//                 <Row>
//                   <HistoriqueCashIn listCashIn={listCashIn} t={t} />
//                 </Row>
//               </Tab>
//             )}
//             {showMenuCashHistorique && (
//               <Tab
//                 eventKey={3}
//                 onSelect={changeIndexTab}
//                 title={t("header.tabTitles.cashOut")}
//               >
//                 <Row>
//                   <HistoriqueCashOut listCashOut={listCashOut} t={t} />
//                 </Row>
//               </Tab>
//             )}
//             {showVirementEDPAgent && (
//               <Tab
//                 eventKey={5}
//                 onSelect={changeIndexTab}
//                 title={t("header.tabTitles.virement")}
//               >
//                 <Row>
//                   <HistoriqueVirement listVirement={listVirement} t={t} />
//                 </Row>
//               </Tab>
//             )}
//             {showMenuTransfertHistorique && (
//               <Tab
//                 eventKey={4}
//                 onSelect={changeIndexTab}
//                 title={t("header.tabTitles.transfert")}
//               >
//                 <Row>
//                   <HistoriqueTransfert listTransfert={listTransfert} t={t} />
//                 </Row>
//               </Tab>
//             )}
//             {showMenuTransfertHistorique && (
//               <Tab
//                 eventKey={6}
//                 onSelect={changeIndexTab}
//                 title={t("header.tabTitles.payment")}
//               >
//                 <Row>
//                   <HistoriquePayment listPayment={listPayment} t={t} />
//                 </Row>
//               </Tab>
//             )}
//             {showUpgradeHistorique && (
//               <Tab eventKey={7} title={t("header.tabTitles.upgrade")}>
//                 <Row>
//                   <HistoriqueUpgrade listUpgrade={listUpgrade} t={t} />
//                 </Row>
//               </Tab>
//             )}
//             {showReclamationHistorique && (
//               <Tab eventKey={8} title={t("header.tabTitles.reclamationClient")}>
//                 <Row>
//                   <HistoriqueReclamation
//                     listReclamation={listReclamation}
//                     t={t}
//                   />
//                 </Row>
//               </Tab>
//             )}
//             {showReclamationHistorique && (
//               <Tab eventKey={9} title={t("header.tabTitles.reclamationAgent")}>
//                 <Row>
//                   <HistoriqueReclamationAgent
//                     listReclamationAgent={listReclamationAgent}
//                     t={t}
//                   />
//                 </Row>
//               </Tab>
//             )}
//             {showCommission && (
//               <Tab eventKey={10} title={t("header.tabTitles.Transactions")}>
//                 <Row>
//                   <HistoriqueTransactions
//                     listTransactions={listTransactions}
//                     t={t}
//                   />
//                 </Row>
//               </Tab>
//             )}
//             {/*{showViderCompte &&*/}
//             {/*< Tab eventKey={11} onSelect={changeIndexTab} title={t('header.tabTitles.viderCompte')}>*/}
//             {/*    <Row>*/}
//             {/*        <HistoriqueViderCompte listViderCompte={listViderCompte} t={t}/>*/}
//             {/*    </Row>*/}
//             {/*</Tab>*/}
//             {/*}*/}
//           </Tabs>
//         </div>
//       </div>
//     );
//   }
// }
// Historique.propTypes = {
//   t: PropTypes.func,
// };
