import React, { Component, PropTypes } from 'react';
import {
    Row,
    Col,
    FormControl,
    ButtonGroup,
    Modal,
    ControlLabel,
    PageHeader
} from 'react-bootstrap';
import { connect } from 'react-redux';
import { reduxForm, initializeWithKey, change } from 'redux-form';
import { asyncConnect } from "redux-async-connect";
import { translate } from 'react-i18next';
import Button from "react-bootstrap-button-loader";
import Attachments from '../../components/Attachement/Attachments'
import * as SouscriptionActions from "./SouscriptionReducer";
import * as AttachementActions from "../../components/Attachement/AttachementReducer";
import { browserHistory } from "react-router";
import { reset } from 'redux-form';
import DatePicker from 'react-datepicker';
import moment from "moment";

import Select from 'react-select';
import 'react-select/dist/react-select.css';
import {AuthorizedComponent} from "react-router-role-authorization";

let SMSerror = true;

export const validate = (values,props) => {
    const errors = {};

    if (!values.nom || !(values.nom.trim())) {
        errors.nom = props.t('form.message.champObligatoire');
    } else if (values.nom.length > 20) {
        errors.nom = 'Le nom saisi est trop long';
    } else if (!/^[a-zA-Z_][a-zA-Z_ ]*[a-zA-Z_]$/i.test(values.nom.trim())) {
        errors.nom = props.t('form.message.saisieIncorrect');
    }
    if (!values.prenom || !(values.prenom.trim())) {
        errors.prenom = props.t('form.message.champObligatoire');
    } else if (values.prenom.length > 20) {
        errors.prenom = 'Le prénom saisi est trop long';
    } else if (!/^[a-zA-Z_][a-zA-Z_ ]*[a-zA-Z_]$/i.test(values.prenom.trim())) {
        errors.prenom = 'Saisie incorrecte ';
    }
    if (!values.gsm || !(values.gsm.trim())) {
        errors.gsm = props.t('form.message.champObligatoire');
    } else if (!/^((\+212|0)[6-7]{1}[0-9]{8})$/i.test(values.gsm) && values.gsm !== '') {
        errors.gsm = props.t('form.message.errTel');
    }
    /*if (!values.email || !(values.email.trim())) {
     errors.email = 'Ce champ est obligatoire';
     }*/
    if (values.email && values.email.trim()) {
        if (!/^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i.test(values.email) && values.email !== '') {
            errors.email = props.t('form.message.errMail');
        }
    }
    if (!values.numeroIdentite || !(values.numeroIdentite.trim())) {
        errors.numeroIdentite = props.t('form.message.champObligatoire');
    } else {
        if (!/^[A-Z]{1,2}[1-9]{1}[0-9]{4,7}$/i.test(values.numeroIdentite) && values.numeroIdentite !== ''){
            errors.numeroIdentite = props.t('form.message.formatInvalid');
        }
        if (values.numeroIdentite && values.numeroIdentite.length > 9) {
            errors.numeroIdentite = props.t('form.message.numLong');
        }
    }
    if (values.typeCode !== 'type_one') {
        if (!values.typePieceIdentite || !(values.typePieceIdentite.trim()) || values.typePieceIdentite === "Choisir le type de votre pièce d'identité :") {
            errors.typePieceIdentite = props.t('form.message.champObligatoire');
        }

        if (values.siron && values.siron === true) {
            if (!values.dateNaissance || !(values.dateNaissance.trim())) {
                errors.dateNaissance = props.t('form.message.champObligatoire');
            } else if (moment(values.dateNaissance).format('DD/MM/YYYY') > moment().subtract(16, "years")) {
                errors.dateNaissance = props.t('form.message.dateInvalid');
            }

            if (!values.dateValidite || !(values.dateValidite.trim())) {
                errors.dateValidite = props.t('form.message.champObligatoire');
            } else if (moment(values.dateValidite).format('DD/MM/YYYY') < moment()) {
                errors.dateValidite = props.t('form.message.dateInvalid');
            }

            if (!values.adresse || !(values.adresse.trim())) {
                errors.adresse = props.t('form.message.nbrCaracteres');
            } else {
                if (/^.{5,}$/i.test(values.adresse)) {
                    if (!/^(?!.*[!@#$%^&*()\-_+={}[\]|\\;:'",<.>\/?]{2}).+$/i.test(values.adresse)) {
                        errors.adresse = props.t('form.message.caracteresSpeciaux');
                    }
                } else {
                    errors.adresse = props.t('form.message.nbrCaracteres');
                }
            }
            if (!values.titre || !(values.titre.trim()) || values.titre === 'Choisir votre titre :') {
                errors.titre = props.t('form.message.champObligatoire');
            }
            if (!values.compteRendu || !(values.compteRendu.trim()) || values.compteRendu === '') {
                errors.compteRendu = props.t('form.message.champObligatoire');
            }
            if (!values.profession || !(values.profession.trim()) || values.profession === 'Choisir votre profession :') {
                errors.profession = props.t('form.message.champObligatoire');
            }
            if (!values.relation || !(values.relation.trim()) || values.relation === "Choisir l'objet de relation :") {
                errors.relation = props.t('form.message.champObligatoire');
            }
            if (!values.pays || !(values.pays.trim()) || values.pays === 'Choisir votre nationalité :') {
                errors.pays = props.t('form.message.champObligatoire');
            }
            if (!values.paysAdresse || !(values.paysAdresse.trim()) || values.paysAdresse === 'Choisir votre pays de résidence :') {
                errors.paysAdresse = props.t('form.message.champObligatoire');
            }
            if (!values.activite || !(values.activite.trim()) || values.activite === 'Choisir votre activité :') {
                errors.activite = props.t('form.message.champObligatoire');
            }
        }
        if (values.typeCode !== 'type_two') {
            if (!values.typeJustifResidence || !(values.typeJustifResidence.trim())) {
                errors.typeJustifResidence = props.t('form.message.champObligatoire');
            }
        }
    }

    if (values.typeCode === 'type_pro') {
        if (!values.transactionType || values.transactionType.length <= 0) {
            errors.transactionType = props.t('form.message.champObligatoire');
        }
        if (!values.mcc || !(values.mcc.trim())) {
            errors.mcc = props.t('form.message.champObligatoire');
        }
        if (!values.codeRaisonSociale || !(values.codeRaisonSociale.trim())) {
            errors.codeRaisonSociale = props.t('form.message.champObligatoire');
        }
        if (!values.nomRaisonSociale || !(values.nomRaisonSociale.trim())) {
            errors.nomRaisonSociale = props.t('form.message.champObligatoire');
        }
        if (!values.numeroPatente || !(values.numeroPatente.trim())) {
            errors.numeroPatente = props.t('form.message.champObligatoire');
        }
        if (!values.codeTribunal || !(values.codeTribunal.trim())) {
            errors.codeTribunal = props.t('form.message.champObligatoire');
        }
    }
    return errors;
};

@asyncConnect([{
    deferred: false,
    promise: ({ params, location, store: { dispatch, getState } }) => {
        dispatch(AttachementActions.resetDocuments());
        dispatch(reset('DemandeSouscription'));
        dispatch(SouscriptionActions.resetShouldReloadForm());
        dispatch(SouscriptionActions.reload());
        dispatch(AttachementActions.reload());
        dispatch(SouscriptionActions.loadTypePack());
        dispatch(SouscriptionActions.loadActivite());
        dispatch(SouscriptionActions.loadTypeIdentite());
        dispatch(SouscriptionActions.loadProfessions());
        dispatch(SouscriptionActions.loadTitles());
        dispatch(SouscriptionActions.loadFondOrigins());
        dispatch(SouscriptionActions.loadTranches());
        dispatch(SouscriptionActions.loadTypeTransaction());
        dispatch(SouscriptionActions.loadPays());
        dispatch(SouscriptionActions.loadObjetRelation());
        dispatch(SouscriptionActions.loadTypeResidence());
        dispatch(SouscriptionActions.isSironActivated());
        if (params.id) {
            return Promise.all([
                dispatch(SouscriptionActions.getSouscriptionInstance(params.id)),
                dispatch(AttachementActions.getDocuments(params.id))
            ]);
        }
    }
}])

@translate(['souscription'], { wait: true })
@reduxForm({
        form: 'DemandeSouscription',
        fields: ['typeCode', 'siron', 'titre', 'compteRendu', 'dateValidite', 'profession', 'transactionType', 'relation', 'adresse', 'pays', 'paysAdresse', 'dateNaissance', 'nom', 'prenom', 'email', 'gsm', 'typePieceIdentite', 'mcc', 'codeRaisonSociale', 'nomRaisonSociale', 'numeroPatente',
            'sourceRevenu', 'tranchesRevenu', 'activite', 'codeTribunal', 'codeOTP', 'typeJustifResidence', 'codeTribunal', 'numeroIdentite'],
        validate,
    },
    state => (

        state.SouscriptionReducer.data && state.SouscriptionReducer.data !== null ?
            {
                initialValues: {
                    typeCode: state.SouscriptionReducer.data.typeCode,
                    siron: state.SouscriptionReducer.showSironFields,
                    nom: state.SouscriptionReducer.data.nom,
                    prenom: state.SouscriptionReducer.data.prenom,
                    email: state.SouscriptionReducer.data.email,
                    gsm: state.SouscriptionReducer.data.gsm,
                    adresse: state.SouscriptionReducer.data.adresse,
                    numeroIdentite: state.SouscriptionReducer.data.numeroIdentite,
                    typePieceIdentite: state.SouscriptionReducer.data.typePieceIdentite,
                    pays: state.SouscriptionReducer.data.pays,
                    paysAdresse: state.SouscriptionReducer.data.paysAdresse,
                    titre: state.SouscriptionReducer.data.titre,
                    transactionType: state.SouscriptionReducer.data.transactionType,
                    relation: state.SouscriptionReducer.data.relation,
                    dateNaissance: state.SouscriptionReducer.data.dateNaissance,
                    dateValidite: state.SouscriptionReducer.data.dateValidite,
                    compteRendu: state.SouscriptionReducer.data.compteRendu,
                    profession: state.SouscriptionReducer.data.profession,
                    mcc: state.SouscriptionReducer.data.mcc,
                    sourceRevenu: state.SouscriptionReducer.data.sourceRevenu,
                    tranchesRevenu: state.SouscriptionReducer.data.tranchesRevenu,
                    activite: state.SouscriptionReducer.data.activite,
                    codeRaisonSociale: state.SouscriptionReducer.data.codeRaisonSociale,
                    nomRaisonSociale: state.SouscriptionReducer.data.nomRaisonSociale,
                    numeroPatente: state.SouscriptionReducer.data.numeroPatente,
                    codeTribunal: state.SouscriptionReducer.data.codeTribunal,
                    codeOTP: '',
                    typeJustifResidence: state.SouscriptionReducer.data.typeJustifResidence,
                    fichiersjoints: state.AttachementReducer.loadedDocuments,
                    isValidated: ''
                },
                open: [false],
                iSuccessContrat: false,
            }
            :
            {
                initialValues: {
                    typeCode: 'type_one',
                    siron: state.SouscriptionReducer.showSironFields,
                    nom: '',
                    prenom: '',
                    email: '',
                    gsm: '',
                    numeroIdentite: '',
                    typePieceIdentite: '',
                    numPasseport: '',
                    mcc: '',
                    sourceRevenu: '',
                    tranchesRevenu: '',
                    activite: '',
                    codeRaisonSociale: '',
                    nomRaisonSociale: '',
                    numeroPatente: '',
                    codeTribunal: '',
                    codeOTP: '',
                    pays: '',
                    paysAdresse: '',
                    compteRendu: '',
                    titre: '',
                    relation: '',
                    dateNaissance: '',
                    dateValidite: '',
                    profession: '',
                    transactionType: [],
                    typeJustifResidence: 'CDR',
                    isValidated: ''
                },
                open: [false],
                iSuccessContrat: false,
            }

    )
)

//Map State To Props
@connect(
    state => ({
        loading: state.SouscriptionReducer.loading,
        loadingFiles: state.AttachementReducer.loading,
        loadingFileContract: state.AttachementReducer.loading,
        contratErrorMsg: state.AttachementReducer.contratErrorMsg,
        SouscriptionSaved: state.SouscriptionReducer.SouscriptionSaved,
        data: state.SouscriptionReducer.data,
        errorMsg: state.SouscriptionReducer.errorMsg,
        isSuccess: state.SouscriptionReducer.isSuccess,
        loadedDocuments: state.AttachementReducer.loadedDocuments,
        loadedContract: state.AttachementReducer.loadedDocuments,
        typesPack: state.SouscriptionReducer.typesPack,
        titleList: state.SouscriptionReducer.titleList,
        paysList: state.SouscriptionReducer.paysList,
        sourceRevenuList: state.SouscriptionReducer.sourceRevenuList,
        tranchesRevenuList: state.SouscriptionReducer.tranchesRevenuList,
        transactionTypeList: state.SouscriptionReducer.transactionTypeList,
        relationList: state.SouscriptionReducer.relationList,
        professionList: state.SouscriptionReducer.professionList,
        activiteList: state.SouscriptionReducer.activiteList,
        typesIdentite: state.SouscriptionReducer.typesIdentite,
        typesJustif: state.SouscriptionReducer.typesJustif,
        isUpdate: state.SouscriptionReducer.isUpdate,
        typeCode: state.SouscriptionReducer.typeCode,
        typePieceIdentite: state.SouscriptionReducer.typePieceIdentite,
        tentativesDepasse: state.SouscriptionReducer.tentativesDepasse,
        shouldReloadForm: state.SouscriptionReducer.shouldReloadForm,
        otpSuccess: state.SouscriptionReducer.otpSuccess,
        isTypeOneNeedAttachments: state.SouscriptionReducer.isTypeOneNeedAttachments,
        statut: state.SouscriptionReducer.statut,
        isUploadSuccessContrat: state.AttachementReducer.isUploadSuccessContrat,
        showSironFields: state.SouscriptionReducer.showSironFields,
        userFrontDetails: state.user.userFrontDetails
    }), { ...SouscriptionActions, ...AttachementActions, initializeWithKey })


export default class SouscriptionType extends AuthorizedComponent {
    constructor(props) {
        super(props);
        this.userRoles = this.props.userFrontDetails.roles;
        this.notAuthorizedPath = baseUrl + 'app/AccessDenied';
        this.getInitialState();
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeNom = this.handleChangeNom.bind(this);
        this.handleChangePrenom = this.handleChangePrenom.bind(this);
        this.handleChangeGsm = this.handleChangeGsm.bind(this);
        this.handleTitreChange = this.handleTitreChange.bind(this);
        this.handleRelationChange = this.handleRelationChange.bind(this);
        this.handlePaysChange = this.handlePaysChange.bind(this);
        this.handleProfessionChange = this.handleProfessionChange.bind(this);
        this.handlePaysAdresseChange = this.handlePaysAdresseChange.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangeSourceRevenu = this.handleChangeSourceRevenu.bind(this);
        this.handleChangeTranchesRevenu = this.handleChangeTranchesRevenu.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleChangePiece = this.handleChangePiece.bind(this);
        this.handleChangetypeJustifResidence = this.handleChangetypeJustifResidence.bind(this);
        this.handleFiles = this.handleFiles.bind(this);
        this.handleChangecodeOTP = this.handleChangecodeOTP.bind(this);
        this.handleRecapValidation = this.handleRecapValidation.bind(this);
        this.handleChangeDateNaissance = this.handleChangeDateNaissance.bind(this);
        this.handleChangeDateValidite = this.handleChangeDateValidite.bind(this);
        this.handleGenerateReport = this.handleGenerateReport.bind(this);
        const _self = this;
        this.filesClickEvents = [_self.close, _self.deleteDoc];
        this.filesClickEvent = [];
    }

    getInitialState() {
        this.state = {
            open: [false],
            isPieceJoint: false,
            istypeJustifResidence: false,
            isCIN: false,
            fichiersjoints: null,
            fichierjointesUpdate: "",
            numeroIdentite: "",
            isType3: false,
            isPro: false,
            isRecap: false,
            nom: "",
            prenom: "",
            gsm: "",
            email: "",
            compteRendu: "",
            codeRaisonSociale: "",
            nomRaisonSociale: "",
            numeroPatente: "",
            mcc: "",
            sourceRevenu: "",
            tranchesRevenu: "",
            activite: "",
            codeTribunal: "",
            codeOTP: "",
            value: "",
            isSucess: false,
            isUpdate: false,
            iSuccessContrat: false,
            oneFile: false,
            typeJustifResidence: "CDR",
            isValidated: '',
            isGenerateReport: true
        };
    }

    componentDidMount() {
        console.log("I'm in did mount");
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.shouldReloadForm !== nextProps.shouldReloadForm && nextProps.shouldReloadForm === true) {
            console.log("Will receive props function");
            this.setState({ typeCode: "type_one" });
            this.setState({
                isPieceJoint: this.props.isTypeOneNeedAttachments,
                isCIN: false,
                istypeJustifResidence: false,
                isPro: false,
                nom: '',
                prenom: '',
                gsm: '',
                email: '',
                adresse: '',
                sourceRevenu: '',
                tranchesRevenu: '',
                relation: '',
                titre: '',
                pays: '',
                paysAdresse: '',
                typePieceIdentite: '',
                profession: '',
                dateNaissance: null,
                dateValidite: null,
                iSuccessContrat: false,
                isValidated: '',
                fichiersjoints: null
            });
            this.props.resetShouldReloadForm();
            this.props.resetDocuments();
        }
    }

    async componentWillMount() {
        this.setState({ iSuccessContrat: false });
        this.setState({ typeCode: this.props.typeCode });

        if (this.props.typeCode !== "type_one" && this.props.typeCode) {
            this.setState({ isPieceJoint: true });
            this.setState({ isCIN: true });
            if (this.props.typeCode !== "type_two") {
                this.setState({ istypeJustifResidence: true });
                if (this.props.typeCode !== "type_three") {
                    this.setState({ isPro: true });
                }
                else {
                    this.setState({ isPro: false });
                }
            }
            else {
                this.setState({ isPro: false, istypeJustifResidence: false });
            }
        }
        else {
            this.setState({
                isCIN: false,
                istypeJustifResidence: false,
                isPro: false
            });
            await this.props.loadTypePackAttachments();
            this.setState({ isPieceJoint: this.props.isTypeOneNeedAttachments });
        }

        this.setState({ typePieceIdentite: this.props.typePieceIdentite });

    }

    handleData() {

    }

    handleChangeDateNaissance(date) {
        this.setState({ dateNaissance: date });
    }

    handleChangeDateValidite(date) {
        this.setState({ dateValidite: date });
    }

    handleTitreChange(event) {
        this.setState({ titre: event.target.value });
    }

    handleRelationChange(event) {
        this.setState({ relation: event.target.value });
    }

    handlePaysChange(event) {
        this.setState({ pays: event.target.value });
    }

    handlePaysAdresseChange(event) {
        this.setState({ paysAdresse: event.target.value });
    }

    handleProfessionChange(event) {
        this.setState({ profession: event.target.value });
    }

    handleChangeNom(event) {
        this.setState({ nom: event.target.value });
    }

    handleChangePrenom(event) {
        this.setState({ prenom: event.target.value });
    }

    handleChangeEmail(event) {
        this.setState({ email: event.target.value });
    }

    handleChangeGsm(event) {
        this.setState({ gsm: event.target.value });
    }

    handleChangeSourceRevenu(event) {
        this.setState({ sourceRevenu: event.target.value });
    }

    handleChangeTranchesRevenu(event) {
        this.setState({ tranchesRevenu: event.target.value });
    }

    handleChangecodeOTP(event) {
        if (/^[0-9]*$/i.test(event.target.value) || event.target.value === '') {
            console.log(event.target.value);
            this.setState({ codeOTP: event.target.value });
            this.handleValidateSMS(event.target.value);
        }
    }

    handleValidateSMS(values) {
        SMSerror = null;
        if (!values) {
            SMSerror = this.props.t('form.message.champObligatoire');
        } else if (values.length > 6 || values.length < 6) {
            SMSerror = this.props.t('form.message.otpValidation');
        }
        return SMSerror;
    }

    async  handleRecap(values) {
        this.state.codeOTP = "";
        if (this.state.fichiersjoints !== null && this.state.isPieceJoint === true && this.state.fichiersjoints.length > 0
            && this.state.fichiersjoints.filter(obj => {
                return obj.isDeleted === false
            }).length > 0) {

            if (values.typeCode === "type_three") {
                if (this.state.fichiersjoints.length >= 1) {
                    await this.props.saveSouscription(values);
                    this.props.uploadDocuments(this.props.data.id, this.state.fichiersjoints, false);
                } else {
                    this.setState({ showModalNumberFilesThree: false });
                }
            } else if (values.typeCode === "type_two") {
                if (this.state.fichiersjoints.length >= 1) {
                    await this.props.saveSouscription(values);
                    this.props.uploadDocuments(this.props.data.id, this.state.fichiersjoints, false);
                } else {
                    this.setState({ showModalNumberFilesTwo: true });
                }
            }
            else {
                if (this.state.fichiersjoints.length >= 1) {
                    await this.props.saveSouscription(values);
                    this.props.uploadDocuments(this.props.data.id, this.state.fichiersjoints, false);
                } else {
                    this.setState({ showModalNumberFilesTwo: true });
                }
            }

            this.setState({ isRecap: true });
            this.setState({ fichiersjoints: null });
        } else {
            if (this.state.isPieceJoint)
                this.setState({ showModal: true });
            else {
                this.props.saveSouscription(values);
                this.setState({ isRecap: true });
            }
        }
    }

    handleGenerateReport(data) {
        this.props.generateReport(data);
    }

    handleResendOtp(dto) {
        this.props.resendOtp(dto.id, dto.gsm);
        this.setState({ codeOTP: "" });
    }

    handleUpdate() {
        this.setState({ isUpdate: true });
        this.setState({ fichiersjoints: null });
        this.props.reset();
    }

    async handleUpdatePersist(values) {
        let attr = values;
        let totalDoc = 0;
        attr.id = this.props.data.id;

        if (this.props.loadedDocuments.length > 0 || (this.state.fichiersjoints !== null
            && this.state.isPieceJoint === true && this.state.fichiersjoints.length > 0
            && this.state.fichiersjoints.filter(obj => {
                return obj.isDeleted === false
            }).length > 0)) {
            if (values.typeCode === "type_three") {
                if (this.state.fichiersjoints !== null) {
                    totalDoc = this.state.fichiersjoints.filter(obj => {
                        return obj.isDeleted === true;
                    }).length;
                    totalDoc = this.state.fichiersjoints.length - totalDoc;
                } else {
                    totalDoc = 0;
                }
                totalDoc = this.props.loadedDocuments.length + totalDoc;

                if (totalDoc >= 1) {
                    await this.props.updateSouscription(attr, attr.id);
                    if (this.state.fichiersjoints !== null && this.state.fichiersjoints.length > 0)
                        this.props.uploadDocuments(attr.id, this.state.fichiersjoints, false);
                }
                else if (totalDoc === 0) {
                    this.setState({ showModalNumberFilesThree: true });
                } else {
                    this.props.updateSouscription(attr, attr.id);
                }
            }
            else if (values.typeCode === "type_two") {
                if (this.state.fichiersjoints !== null) {
                    totalDoc = this.state.fichiersjoints.filter(obj => {
                        return obj.isDeleted === true;
                    }).length;
                    totalDoc = this.state.fichiersjoints.length - totalDoc;
                } else {
                    totalDoc = 0;
                }

                totalDoc = this.props.loadedDocuments.length + totalDoc;

                if (totalDoc >= 1) {
                    await this.props.updateSouscription(attr, attr.id);
                    if (this.state.fichiersjoints !== null && this.state.fichiersjoints.length > 0)
                        this.props.uploadDocuments(attr.id, this.state.fichiersjoints, false);
                }
                else if (totalDoc === 0) {
                    this.setState({ showModalNumberFilesTwo: true });
                } else {
                    this.props.updateSouscription(attr, attr.id);
                }
            }
            else {
                if (this.state.fichiersjoints !== null) {
                    totalDoc = this.state.fichiersjoints.filter(obj => {
                        return obj.isDeleted === true;
                    }).length;
                    totalDoc = this.state.fichiersjoints.length - totalDoc;
                } else {
                    totalDoc = 0;
                }

                totalDoc = this.props.loadedDocuments.length + totalDoc;
                if (totalDoc  >= 1) {
                    await this.props.updateSouscription(attr, attr.id);
                    if (this.state.fichiersjoints !== null && this.state.fichiersjoints.length > 0)
                        this.props.uploadDocuments(attr.id, this.state.fichiersjoints, false);
                } else {
                    this.setState({ showModalNumberFilesTwo: true });
                }
            }

        } else {
            if (this.state.isPieceJoint)
                this.setState({ showModal: true });
            else {
                this.props.updateSouscription(attr, attr.id);
            }
        }
    }

    handleReject() {
        this.setState({ isRecap: false });
        this.getInitialState();
        this.props.reset();
        this.props.resetDocuments();
    }

    async handleRecapValidation(data) {
        console.log("Handle Validation !");
        await this.props.sendOtp(data.id, data.gsm);
        if (this.props.otpSuccess === true)
            this.setState({ isValidated: true });
    }

    async handleSave(data, values, id) {
        await this.props.submitSouscription(data, values, id);

        if (this.props.isSuccess === false)
            this.setState({ isValidated: true });
        else
            this.setState({ isValidated: false });
    }

    handleChange(event) {
        this.setState({ typeCode: event.target.value });
        event.target.selected = true;

        this.setState({ isPieceJoint: this.props.isTypeOneNeedAttachments });

        if (event.target.value !== "type_one") {
            this.setState({ isPieceJoint: true });
            this.setState({ isCIN: true });
            this.setState({ isGenerateReport: true });
            if (event.target.value !== "type_two") {
                this.setState({ istypeJustifResidence: true });
                if (event.target.value !== "type_three") {
                    this.setState({ isPro: true });
                }
                else {
                    this.setState({ isPro: false });
                }
            }
            else {
                this.setState({ isPro: false, istypeJustifResidence: false });
            }
        }
        else {
            this.setState({
                isCIN: false,
                isPieceJoint: this.props.isTypeOneNeedAttachments,
                isnumPasseport: false,
                istypeJustifResidence: false,
                isGenerateReport: true,
                isPro: false
            });
        }
    }

    handleChangePiece(event) {
        if (event.target.value === "MAR") {
            this.setState({ pays: "MA" });
            this.setState({ paysAdresse: "MA" });
            this.props.dispatch(change('DemandeSouscription', 'pays', 'MA'));
            this.props.dispatch(change('DemandeSouscription', 'paysAdresse', 'MA'));
        }
        this.setState({ typePieceIdentite: event.target.value });
    }

    handleChangetypeJustifResidence(event) {
        this.setState({ typeJustifResidence: event.target.value });
    }


    handleFiles(files) {
        this.setState({
            fichiersjoints: files
        });
    }

    onChange(e) {
        this.setState({
            gsm: e.target.value
        });

        console.log("The Input Text has changed to ", e.target.value);
    }

    onSelect(val) {
        this.setState({
            gsm: val
        });

        console.log("Option from 'database' selected : ", val);
    }

    async handleSaveContrat() {
        let total = 0;
        this.setState({ oneFile: false });
        if (this.state.fichiersjoints !== null) {
            total = this.state.fichiersjoints.length - total;
            console.log(total);
            if (total >= 1) {
                console.log(this.props.data.id);
                await this.props.uploadDocuments(this.props.data.id, this.state.fichiersjoints, true);
                if (this.props.isUploadSuccessContrat) {
                    this.props.getSouscriptionInstance(this.props.data.id);
                    this.setState({ iSuccessContrat: true });
                }
            }
            else {
                this.setState({ showModalNumberFilesThreeRattachement: true });
            }
        } else {
            this.setState({ showModalNumberFilesThreeRattachement: true });
        }
        window.scrollTo(0, -500);
    }

    handleSelect(i) {
        let array = this.state.open;
        array[i] = !this.state.open[i];
        this.setState({ open: array });
    }

    render() {
        const close = () => {
            this.setState({
                showModal: false,
                oneFile: false,
                showModalNumberFilesThree: false,
                showModalNumberFilesThreeRattachement: false,
                showModalNumberFilesTwo: false
            });
        };
        const {
            t, resetForm, otpSuccess, isTypeOneNeedAttachments, statut, showSironFields, titleList, professionList, relationList, paysList, transactionTypeList, isUpdate,
            saveSouscription, uploadDocuments, submitSouscription, errorMsg, isSuccess, activiteList, typesPack, generateReport,
            typesIdentite, sourceRevenuList, tranchesRevenuList, typesJustif, SouscriptionSaved, loadedDocuments, SouscriptionUpdated, reset, updateSouscription,
            values, handleSubmit, data, fields: {
                typeCode, nom, prenom, email, compteRendu, gsm, siron, adresse, paysAdresse, numeroIdentite, mcc, activite, sourceRevenu, tranchesRevenu, codeRaisonSociale
                , nomRaisonSociale, numeroPatente, codeTribunal, codeOTP, transactionType, dateNaissance, dateValidite, relation, pays, titre,
                profession, typeJustifResidence, typePieceIdentite
            }, loading, loadingFiles, contratErrorMsg, loadingFileContract, files, tentativesDepasse
        } = this.props;

        const styles = require('../Souscription/Souscription.scss');
        return (
            <div>
                <Modal show={loading} className="loadingModal" backdrop="static" keyboard={false}>
                    <Modal.Body>
                        <Row>
                            <Col xs={12} md={12}>
                                <div className="spinner">
                                    <span style={{ fontSize: '11px' }}>{t('chargement.title')}</span>
                                </div>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>

                <Modal show={loadingFiles} className="loadingModal" backdrop="static" keyboard={false}>
                    <Modal.Body>
                        <Row>
                            <Col xs={12} md={12}>
                                <div className="spinner">
                                    <span style={{ fontSize: '11px' }}>{t('chargement.title')}</span>
                                </div>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal show={loadingFileContract} className="loadingModal" backdrop="static" keyboard={false}>
                    <Modal.Body>
                        <Row>
                            <Col xs={12} md={12}>
                                <div className="spinner">
                                    <span style={{ fontSize: '11px' }}>{t('chargement.title')}</span>
                                </div>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Modal
                    show={this.state.showModalNumberFilesThreeRattachement}
                    onHide={close}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">
                            <div>{t('popup.uploadFiles.title')}</div>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div>{t('popup.uploadFiles.msg')} </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <ButtonGroup
                            className="pull-right" bsSize="small"
                        >
                            <Button className={styles.ButtonPasswordStyle}
                                    onClick={() => close()}>{t('popup.uploadFiles.noBtn')}</Button>
                        </ButtonGroup>
                    </Modal.Footer>
                </Modal>

                {(SouscriptionSaved === null || SouscriptionSaved === false) &&
                <div>

                    <PageHeader>
                        <h3>{t('form.titleForm.souscription')}</h3>
                    </PageHeader>

                    {errorMsg && errorMsg !== "" && SouscriptionSaved === false &&
                    <div className="alert alert-danger">
                        {errorMsg}
                    </div>
                    }

                    <Row>
                        <form className="formContainer">
                            <Row className={styles.fieldRow}>
                                <Col xs="12" md="12">
                                    <ControlLabel>{t('form.label.pack')}</ControlLabel>

                                    {showSironFields && showSironFields === true &&
                                    <FormControl type="hidden"  {...siron}
                                                 value={true} className={styles.datePickerFormControl} />
                                    }

                                    <FormControl componentClass="select" {...typeCode}
                                                 ref="typeCodeList"
                                                 onChange={this.handleChange}
                                                 value={this.state.typeCode ? this.state.typeCode : "type_one"}
                                                 className={styles.datePickerFormControl}>
                                        {
                                            typesPack && typesPack.length && typesPack.map((typeP) =>
                                                <option
                                                    key={typeP.id}
                                                    value={typeP.code}
                                                >{typeP.libelle}</option>
                                            )
                                        }

                                    </FormControl>
                                </Col>
                            </Row>

                            <fieldset style={{ marginTop: '20px' }}>

                                <legend>{t('form.legend.infoperso')}</legend>
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.nom')}</ControlLabel>
                                        <FormControl type="text"  {...nom}
                                                     className={styles.datePickerFormControl} />
                                        {nom.error && nom.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{nom.error}</i>
                                        </div>
                                        }
                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.prenom')} </ControlLabel>
                                        <FormControl type="text" {...prenom}
                                                     className={styles.datePickerFormControl} />
                                        {prenom.error && prenom.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{prenom.error}</i>
                                        </div>
                                        }
                                    </Col>
                                </Row>

                                <Row className={styles.fieldRow}>

                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.phone')}</ControlLabel>
                                        <FormControl type="text" {...gsm}
                                                     className={styles.datePickerFormControl} />
                                        {gsm.error && gsm.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{gsm.error}</i>
                                        </div>
                                        }
                                    </Col>


                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.mail')} </ControlLabel>
                                        <FormControl type="text" {...email}
                                                     className={styles.datePickerFormControl} />
                                        {email.error && email.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{email.error}</i>
                                        </div>
                                        }
                                    </Col>
                                </Row>
                                {this.state.typeCode !== 'type_one' && showSironFields &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.titre')}</ControlLabel>
                                        <FormControl componentClass="select" {...titre}
                                                     ref="typeCodeList"
                                                     onChange={this.handleTitreChange}
                                                     value={this.state.titre ? this.state.titre : ""}
                                                     className={styles.datePickerFormControl}>
                                            <option hidden>Choisir le titre :</option>
                                            {
                                                titleList && titleList.length && titleList.map((titleP) =>
                                                    <option
                                                        key={titleP.id}
                                                        value={titleP.code}
                                                    >{titleP.libelle}</option>
                                                )
                                            }

                                        </FormControl>
                                        {titre.error && titre.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{titre.error}</i>
                                        </div>
                                        }
                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.dateNaissance')}</ControlLabel>
                                        <span {...dateNaissance}>
                                                    <DatePicker
                                                        maxDate={moment().subtract(16, "years")}
                                                        selected={this.state.dateNaissance}
                                                        dateNaissance={this.state.dateNaissance}
                                                        placeholderText={t('form.label.dateNaissance')}
                                                        onChange={this.handleChangeDateNaissance}
                                                        className={styles.datePickerFormControl}
                                                        isClearable="true"
                                                        locale="fr-FR"
                                                        dateFormat="DD/MM/YYYY"
                                                    />
                                                </span>
                                        {dateNaissance.error && dateNaissance.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{dateNaissance.error}</i>
                                        </div>
                                        }
                                    </Col>
                                </Row>
                                }

                                {this.state.typeCode !== 'type_one' && showSironFields &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.adresse')}:</ControlLabel>
                                        <FormControl type="text"  {...adresse}
                                                     className={styles.datePickerFormControl} />

                                        {adresse.error && adresse.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{adresse.error}</i>
                                        </div>
                                        }

                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.paysAdresse')}</ControlLabel>
                                        <FormControl componentClass="select" {...paysAdresse}
                                                     ref="typeCodeList"
                                                     onChange={this.handlePaysAdresseChange}
                                                     value={this.state.paysAdresse ? this.state.paysAdresse : ""}
                                                     className={styles.datePickerFormControl}>
                                            <option hidden>Choisir le pays de résidence :</option>
                                            {
                                                paysList && paysList.length && paysList.map((paysP) =>
                                                    <option
                                                        key={paysP.id}
                                                        value={paysP.code}
                                                    >{paysP.libelle}</option>
                                                )
                                            }

                                        </FormControl>
                                        {paysAdresse.error && paysAdresse.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{paysAdresse.error}</i>
                                        </div>
                                        }
                                    </Col>
                                </Row>
                                }

                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.pieceIdentite')}</ControlLabel>
                                        <FormControl componentClass="select" {...typePieceIdentite}
                                                     onChange={this.handleChangePiece}
                                                     value={this.state.typePieceIdentite ? this.state.typePieceIdentite : typePieceIdentite.value}
                                                     className={styles.datePickerFormControl}>
                                            <option hidden>{t('form.label.typePieceIdentite')} </option>
                                            {
                                                typesIdentite && typesIdentite.length && typesIdentite.map((typeP) =>
                                                    <option
                                                        key={typeP.id}
                                                        value={typeP.code}
                                                    >{typeP.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {typePieceIdentite.error && typePieceIdentite.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{typePieceIdentite.error}</i>
                                        </div>
                                        }
                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.cin')} </ControlLabel>
                                        <FormControl name='cin' type="text"  {...numeroIdentite}
                                                     className={styles.datePickerFormControl}
                                        />
                                        {numeroIdentite.error && numeroIdentite.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{numeroIdentite.error}</i>
                                        </div>
                                        }
                                    </Col>
                                </Row>

                                {this.state.typeCode !== 'type_one' && showSironFields &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.dateValidite')}</ControlLabel>
                                        <span {...dateValidite}>
                                                    <DatePicker
                                                        minDate={moment()}
                                                        selected={this.state.dateValidite}
                                                        dateValidite={this.state.dateValidite}
                                                        placeholderText={t('form.label.dateValidite')}
                                                        onChange={this.handleChangeDateValidite}
                                                        className={styles.datePickerFormControl}
                                                        isClearable="true"
                                                        locale="fr-FR"
                                                        dateFormat="DD/MM/YYYY"
                                                    />
                                                </span>

                                        {dateValidite.error && dateValidite.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{dateValidite.error}</i>
                                        </div>
                                        }
                                    </Col>

                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.pays')}</ControlLabel>
                                        <FormControl componentClass="select" {...pays}
                                                     ref="typeCodeList"
                                                     onChange={this.handlePaysChange}
                                                     value={this.state.pays ? this.state.pays : ""}
                                                     className={styles.datePickerFormControl}>
                                            <option hidden>Choisir votre nationalité :</option>
                                            {
                                                paysList && paysList.length && paysList.map((paysP) =>
                                                    <option
                                                        key={paysP.id}
                                                        value={paysP.code}
                                                    >{paysP.libelle}</option>
                                                )
                                            }

                                        </FormControl>
                                        {pays.error && pays.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{pays.error}</i>
                                        </div>
                                        }

                                    </Col>
                                </Row>
                                }


                                {this.state.typeCode !== 'type_one' && showSironFields &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.profession')}</ControlLabel>
                                        <FormControl componentClass="select" {...profession}
                                                     ref="typeCodeList"
                                                     onChange={this.handleProfessionChange}
                                                     value={this.state.profession ? this.state.profession : ""}
                                                     className={styles.datePickerFormControl}>
                                            <option hidden>Choisir votre profession :</option>
                                            {
                                                professionList && professionList.length && professionList.map((professionP) =>
                                                    <option
                                                        key={professionP.id}
                                                        value={professionP.code}
                                                    >{professionP.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {profession.error && profession.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{profession.error}</i>
                                        </div>
                                        }
                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.relation')}</ControlLabel>
                                        <FormControl componentClass="select" {...relation}
                                                     onChange={this.handleRelationChange}
                                                     value={this.state.relation ? this.state.relation : ""}
                                                     className={styles.datePickerFormControl}>
                                            <option hidden>Choisir l'objet de relation :</option>
                                            {
                                                relationList && relationList.length && relationList.map((relationP) =>
                                                    <option
                                                        key={relationP.id}
                                                        value={relationP.code}
                                                    >{relationP.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {relation.error && relation.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{relation.error}</i>
                                        </div>
                                        }
                                    </Col>
                                </Row>
                                }

                                {this.state.typeCode !== 'type_one' && showSironFields &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.compteRendu')}</ControlLabel>
                                        <FormControl type="text" {...compteRendu}
                                                     className={styles.datePickerFormControl} />
                                        {compteRendu.error && compteRendu.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{compteRendu.error}</i>
                                        </div>
                                        }

                                    </Col>
                                </Row>
                                }

                                {(sourceRevenuList && sourceRevenuList.length > 0) && showSironFields && this.state.typeCode === 'type_three' &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel> {t('form.label.revenu')}</ControlLabel>
                                        <FormControl  {...sourceRevenu} componentClass="select"
                                                      onChange={this.handleChangeSourceRevenu}
                                                      className={styles.datePickerFormControl}
                                                      value={this.state.sourceRevenu ? this.state.sourceRevenu : sourceRevenu.value}
                                        >
                                            <option hidden>Choisir votre source de revenu :</option>
                                            {
                                                sourceRevenuList && sourceRevenuList.length && sourceRevenuList.map((typeP) =>
                                                    <option
                                                        key={typeP.id}
                                                        value={typeP.code}
                                                    >{typeP.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {sourceRevenu.error && sourceRevenu.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{sourceRevenu.error}</i>
                                        </div>
                                        }
                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel> {t('form.label.tranchesRevenu')}</ControlLabel>
                                        <FormControl  {...tranchesRevenu} componentClass="select"
                                                      onChange={this.handleChangeTranchesRevenu}
                                                      className={styles.datePickerFormControl}
                                                      value={this.state.tranchesRevenu ? this.state.tranchesRevenu : tranchesRevenu.value}
                                        >
                                            <option hidden>Choisir votre tranches :</option>
                                            {
                                                tranchesRevenuList && tranchesRevenuList.length && tranchesRevenuList.map((typeP) =>
                                                    <option
                                                        key={typeP.id}
                                                        value={typeP.code}
                                                    >{typeP.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {tranchesRevenu.error && tranchesRevenu.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{tranchesRevenu.error}</i>
                                        </div>
                                        }
                                    </Col>
                                </Row>
                                }

                                {this.state.istypeJustifResidence === true && this.state.typeCode !== "type_two" &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel> {t('form.label.justificatif')}</ControlLabel>
                                        <FormControl  {...typeJustifResidence} componentClass="select"
                                                      onChange={this.handleChangetypeJustifResidence}
                                                      className={styles.datePickerFormControl}
                                                      value={this.state.typeJustifResidence ? this.state.typeJustifResidence : typeJustifResidence.value}
                                        >
                                            {
                                                typesJustif && typesJustif.length && typesJustif.map((typeP) =>
                                                    <option
                                                        key={typeP.id}
                                                        value={typeP.code}
                                                    >{typeP.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {typeJustifResidence.error && typeJustifResidence.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{typeJustifResidence.error}</i>
                                        </div>
                                        }
                                    </Col>

                                    {!showSironFields &&
                                    <Col xs="12" md="6">
                                        <ControlLabel> {t('form.label.revenu')}</ControlLabel>
                                        <FormControl type="text"  {...sourceRevenu}
                                                     className={styles.datePickerFormControl} />

                                        {sourceRevenu.error && sourceRevenu.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{sourceRevenu.error}</i>
                                        </div>
                                        }
                                    </Col>
                                    }

                                    {showSironFields &&
                                    <Col xs="12" md="6">
                                        <ControlLabel> {t('form.label.activite')}</ControlLabel>
                                        <FormControl  {...activite} componentClass="select"
                                                      className={styles.datePickerFormControl}>
                                            <option hidden>Sélectionner l'activité professionnelle du client
                                            </option>
                                            {
                                                activiteList && activiteList.length && activiteList.map((activite) =>
                                                    <option
                                                        key={activite.id}
                                                        value={activite.code}
                                                    >{activite.libelle}</option>
                                                )
                                            }
                                        </FormControl>
                                        {activite.error && activite.touched &&
                                        <div className={styles.error}>
                                            <i className="fa fa-exclamation-triangle"
                                               aria-hidden="true">{activite.error}</i>
                                        </div>
                                        }
                                    </Col>
                                    }
                                </Row>
                                }
                            </fieldset>
                            {this.state.isPro === true &&
                            <Row className={styles.fieldRow}>
                                <Col xs="12" md="6">
                                    <ControlLabel>{t('form.label.RaisonSocial')} </ControlLabel>
                                    <FormControl {...nomRaisonSociale} type="text"
                                                 className={styles.datePickerFormControl} />
                                    {nomRaisonSociale.error && nomRaisonSociale.touched &&
                                    <div className={styles.error}>
                                        <i className="fa fa-exclamation-triangle"
                                           aria-hidden="true">{nomRaisonSociale.error}</i>
                                    </div>
                                    }
                                </Col>
                                <Col xs="12" md="6">
                                    <ControlLabel>{t('form.label.CodeSocial')} </ControlLabel>
                                    <FormControl  {...codeRaisonSociale} type="text"
                                                  className={styles.datePickerFormControl}
                                    />
                                    {codeRaisonSociale.error && codeRaisonSociale.touched &&
                                    <div className={styles.error}>
                                        <i className="fa fa-exclamation-triangle"
                                           aria-hidden="true">{codeRaisonSociale.error}</i>
                                    </div>
                                    }
                                </Col>
                            </Row>
                            }
                            {this.state.isPro === true &&
                            <Row className={styles.fieldRow}>
                                <Col xs="12" md="6">
                                    <ControlLabel> {t('form.label.NumPatente')}</ControlLabel>
                                    <FormControl {...numeroPatente} type="text"
                                                 className={styles.datePickerFormControl}
                                    />
                                    {numeroPatente.error && numeroPatente.touched &&
                                    <div className={styles.error}>
                                        <i className="fa fa-exclamation-triangle"
                                           aria-hidden="true">{numeroPatente.error}</i>
                                    </div>
                                    }
                                </Col>
                                <Col xs="12" md="6">
                                    <ControlLabel> {t('form.label.mcc')}</ControlLabel>
                                    <FormControl  {...mcc} componentClass="select"
                                                  className={styles.datePickerFormControl}>
                                        <option value="" hidden> mcc</option>
                                        <option value="Restaurant"> Restaurant</option>
                                        <option value="Service éducation">Service éducation</option>
                                        <option value="Magasin">Magasin</option>
                                    </FormControl>
                                    {mcc.error && mcc.touched &&
                                    <div className={styles.error}>
                                        <i className="fa fa-exclamation-triangle"
                                           aria-hidden="true">{mcc.error}</i>
                                    </div>
                                    }
                                </Col>
                            </Row>
                            }
                            {this.state.isPro === true &&

                            <Row className={styles.fieldRow}>
                                <Col xs="12" md="6">
                                    <ControlLabel>  {t('form.label.CodeTribunal')}</ControlLabel>
                                    <FormControl type="text" {...codeTribunal}
                                                 className={styles.datePickerFormControl}
                                    />
                                    {codeTribunal.error && codeTribunal.touched &&
                                    <div className={styles.error}>
                                        <i className="fa fa-exclamation-triangle"
                                           aria-hidden="true">{codeTribunal.error}</i>
                                    </div>
                                    }
                                </Col>
                            </Row>
                            }

                            <fieldset style={{ marginTop: '20px' }}>

                                <form ref="form">
                                    {this.state.isPieceJoint === true &&
                                    <div>
                                        <legend>{t('form.legend.piecejoints')}</legend>
                                        <DownloadedFiles t={t} open={open} files={this.props.loadedDocuments}
                                                         handleSelect={this.handleSelect}
                                                         filesClickEvents={this.filesClickEvents}
                                                         handleFiles={this.handleFiles}
                                                         fichiersjoints={this.state.fichiersjoints}
                                                         disableALL={this.state.disableALL}
                                                         isConsultOnly={false}
                                        />
                                        <Modal
                                            show={this.state.oneFile}
                                            onHide={close}
                                            container={this}
                                            aria-labelledby="contained-modal-title"
                                        >
                                            <Modal.Header closeButton>
                                                <Modal.Title id="contained-modal-title">
                                                    <div>{t('popup.uploadFiles.title')}</div>
                                                </Modal.Title>
                                            </Modal.Header>
                                            <Modal.Body>
                                                <div>{t('popup.uploadContrat.msg')}</div>
                                            </Modal.Body>
                                            <Modal.Footer>
                                                <ButtonGroup
                                                    className="pull-right" bsSize="small"
                                                >
                                                    <Button className={styles.ButtonPasswordStyle}
                                                            onClick={() => close()}>{t('popup.uploadFiles.noBtn')}</Button>
                                                </ButtonGroup>
                                            </Modal.Footer>
                                        </Modal>
                                        <Modal
                                            show={this.state.showModal}
                                            onHide={close}
                                            container={this}
                                            aria-labelledby="contained-modal-title"
                                        >
                                            <Modal.Header closeButton>
                                                <Modal.Title id="contained-modal-title">
                                                    <div>{t('popup.uploadFiles.title')}</div>
                                                </Modal.Title>
                                            </Modal.Header>
                                            <Modal.Body>
                                                <div>{t('popup.uploadFiles.msg')}</div>
                                            </Modal.Body>
                                            <Modal.Footer>
                                                <ButtonGroup
                                                    className="pull-right" bsSize="small"
                                                >
                                                    <Button className={styles.ButtonPasswordStyle}
                                                            onClick={() => close()}>{t('popup.uploadFiles.noBtn')}</Button>
                                                </ButtonGroup>
                                            </Modal.Footer>
                                        </Modal>
                                        <Modal
                                            show={this.state.showModalNumberFilesThree}
                                            onHide={close}
                                            container={this}
                                            aria-labelledby="contained-modal-title"
                                        >
                                            <Modal.Header closeButton>
                                                <Modal.Title id="contained-modal-title">
                                                    <div>{t('popup.uploadFiles.title')}</div>
                                                </Modal.Title>
                                            </Modal.Header>
                                            <Modal.Body>
                                                <div>Merci de joindre les fichiers demandés</div>
                                            </Modal.Body>
                                            <Modal.Footer>
                                                <ButtonGroup
                                                    className="pull-right" bsSize="small"
                                                >
                                                    <Button className={styles.ButtonPasswordStyle}
                                                            onClick={() => close()}>{t('popup.uploadFiles.noBtn')}</Button>
                                                </ButtonGroup>
                                            </Modal.Footer>
                                        </Modal>
                                        <Modal
                                            show={this.state.showModalNumberFilesTwo}
                                            onHide={close}
                                            container={this}
                                            aria-labelledby="contained-modal-title"
                                        >
                                            <Modal.Header closeButton>
                                                <Modal.Title id="contained-modal-title">
                                                    <div>{t('popup.uploadFiles.title')}</div>
                                                </Modal.Title>
                                            </Modal.Header>
                                            <Modal.Body>
                                                <div>Merci de joindre les fichiers demandés</div>
                                            </Modal.Body>
                                            <Modal.Footer>
                                                <ButtonGroup
                                                    className="pull-right" bsSize="small"
                                                >
                                                    <Button className={styles.ButtonPasswordStyle}
                                                            onClick={() => close()}>{t('popup.uploadFiles.noBtn')}</Button>
                                                </ButtonGroup>
                                            </Modal.Footer>
                                        </Modal>
                                    </div>}

                                    <Row className="pull-right">
                                        {!isUpdate ?
                                            <Col xs="12" md="12">
                                                <Button onClick={handleSubmit(() => {
                                                    this.handleRecap(values);
                                                })} bsStyle="primary">
                                                    <i className="fa fa-check " /> {t('form.buttons.save')}
                                                </Button>
                                            </Col>
                                            :
                                            <Col xs="12" md="12" className="pull-right">
                                                <Button onClick={handleSubmit(() => {
                                                    this.handleUpdatePersist(values);
                                                })} bsStyle="primary">
                                                    <i className="fa fa-check " /> {t('form.buttons.save')}
                                                </Button>
                                            </Col>
                                        }
                                    </Row>

                                </form>

                            </fieldset>


                        </form>


                    </Row>

                </div>
                }
                {((SouscriptionSaved && isSuccess !== true) || this.state.iSuccessContrat === true) &&
                <div>
                    <Row>
                        <Col xs="12" md="12">
                            <h2>{t('form.titleForm.ConfirmationOuvertureCompte')}</h2>
                        </Col>
                    </Row>
                    {this.state.iSuccessContrat === true &&
                    <div className="alert alert-success">
                        {t('block.msg.success')}
                    </div>
                    }

                    {errorMsg && errorMsg !== "" && isSuccess === false &&
                    <div className="alert alert-danger">
                        {errorMsg}
                    </div>
                    }

                    {contratErrorMsg && contratErrorMsg !== "" && this.state.iSuccessContrat === false &&
                    <div className="alert alert-danger">
                        {contratErrorMsg}
                    </div>
                    }

                    <Row>
                        <form className="formContainer">
                            <Row className={styles.fieldRow}>
                                <Col xs="12" md="12">
                                    <ControlLabel>{t('form.label.pack')}</ControlLabel>

                                    <FormControl componentClass="select" {...typeCode}
                                                 className={styles.datePickerFormControl}
                                                 disabled={this.state.isRecap}
                                                 value={data.typeCode}>
                                        {
                                            typesPack && typesPack.length && typesPack.map((typeP) =>
                                                <option
                                                    key={typeP.id}
                                                    value={typeP.code}
                                                >{typeP.libelle}</option>
                                            )
                                        }

                                    </FormControl>
                                </Col>
                            </Row>

                            <fieldset style={{ marginTop: '20px' }}>
                                <legend>{t('form.legend.infoperso')}</legend>
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.nom')}</ControlLabel>
                                        <p className="detail-value">{data.nom}</p>

                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.prenom')} </ControlLabel>
                                        <p className="detail-value">{data.prenom}</p>
                                    </Col>
                                </Row>

                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.phone')} </ControlLabel>
                                        <p className="detail-value">{data.gsm}</p>
                                    </Col>
                                    {data.email !== '' && data.email !== null &&
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.mail')} </ControlLabel>
                                        <p className="detail-value">{data.email}</p>
                                    </Col>
                                    }
                                </Row>

                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.cin')} </ControlLabel>
                                        <p className="detail-value">{data.numeroIdentite}</p>
                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.typePiece')} </ControlLabel>
                                        {data.typePieceIdentite === 'MAR' && <p className="detail-value">{t('form.label.cin1')}</p>}
                                        {data.typePieceIdentite === 'CSJ' && <p className="detail-value">{t('form.label.carteSejour1')}</p>}
                                        {data.typePieceIdentite === 'CDP' && <p className="detail-value">{t('form.label.carteDiplomatique')}</p>}
                                        {data.typePieceIdentite === 'PBM' && <p className="detail-value">{t('form.label.passeport')}</p>}
                                    </Col>
                                </Row>

                                {data.typeCode !== 'type_one' && showSironFields &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.dateValidite')} :</ControlLabel>
                                        <p className="detail-value">{data.dateValidite}</p>

                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.titre')} :</ControlLabel>
                                        <p className="detail-value">{data.titreLibelle}</p>

                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.profession')} :</ControlLabel>
                                        <p className="detail-value">{data.professionLibelle}</p>

                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.dateNaissance')} :</ControlLabel>
                                        <p className="detail-value">{data.dateNaissance}</p>

                                    </Col>
                                </Row>
                                }

                                {data.typeCode !== 'type_one' && showSironFields &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.pays')} :</ControlLabel>
                                        <p className="detail-value">{data.paysLibelle}</p>

                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.adresse')} :</ControlLabel>
                                        <p className="detail-value">{data.adresse}</p>

                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.paysAdresse')} :</ControlLabel>
                                        <p className="detail-value">{data.paysAdresseLibelle}</p>

                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.relation')} :</ControlLabel>
                                        <p className="detail-value">{data.relationLibelle}</p>
                                    </Col>
                                </Row>
                                }

                                {data.typeCode === 'type_three' && showSironFields &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.revenu')} :</ControlLabel>
                                        <p className="detail-value">{data.sourceRevenuLibelle}</p>

                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.compteRendu')} :</ControlLabel>
                                        <p className="detail-value">{data.compteRendu}</p>

                                    </Col>
                                </Row>
                                }

                                {this.state.istypeJustifResidence === true &&
                                <Row className={styles.fieldRow}>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.justificatif')}</ControlLabel>
                                        <p className="detail-value">{data.justifResidence}</p>
                                    </Col>
                                    <Col xs="12" md="6">
                                        <ControlLabel>{t('form.label.revenu')}</ControlLabel>
                                        <p className="detail-value">{data.sourceRevenu}</p>
                                    </Col>
                                </Row>
                                }


                            </fieldset>
                            {this.state.isPro === true &&
                            <Row className={styles.fieldRow}>
                                <Col xs="12" md="6">
                                    <ControlLabel>{t('form.label.RaisonSocial')} </ControlLabel>
                                    <p className="detail-value">{data.nomRaisonSociale}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <ControlLabel>{t('form.label.CodeSocial')} </ControlLabel>
                                    <p className="detail-value">{data.codeRaisonSociale}</p>
                                </Col>
                            </Row>
                            }
                            {this.state.isPro === true &&
                            <Row className={styles.fieldRow}>
                                <Col xs="12" md="6">
                                    <ControlLabel>  {t('form.label.NumPatente')} </ControlLabel>
                                    <p className="detail-value">{data.numeroPatente}</p>
                                </Col>
                                <Col xs="12" md="6">
                                    <ControlLabel> mcc: </ControlLabel>
                                    <p className="detail-value">{data.mcc}</p>
                                </Col>
                            </Row>
                            }
                            {this.state.isPro === true &&
                            <Row className={styles.fieldRow}>
                                <Col xs="12" md="6">
                                    <ControlLabel>  {t('form.label.mcc')} </ControlLabel>
                                    <p className="detail-value">{data.codeTribunal}</p>
                                </Col>
                            </Row>
                            }

                            <Row className={styles.fieldRow}>
                                <Col xs="12" md="6">
                                    <ControlLabel> {t('form.label.statut')}</ControlLabel>
                                    <p className="detail-value">{statut && statut !== null ? statut : data.statut}</p>
                                </Col>
                            </Row>

                            <fieldset style={{ marginTop: '20px' }}>

                                <form ref="form">
                                    {this.state.isPieceJoint === true && isSuccess !== true &&
                                    <div>
                                        <legend>{t('form.legend.piecejoints')}</legend>
                                        <DownloadedFiles t={t} open={open} files={this.props.loadedDocuments}
                                                         handleSelect={this.handleSelect}
                                                         filesClickEvent={this.filesClickEvent}
                                                         fichiersjoints={this.state.fichiersjoints}
                                                         disableALL={this.state.disableALL}
                                                         isConsultOnly={true}
                                        />
                                    </div>
                                    }
                                    {(isSuccess !== true && tentativesDepasse !== true && data.statutCode !== "Emis") &&
                                    <div style={{ marginTop: '40px' }}>
                                        {this.state.isValidated && this.state.isValidated === true &&
                                        <Row className={styles.fieldRow}>
                                            <legend>{t('form.legend.sms')}</legend>
                                            <Col xs="4" md="4">
                                                <ControlLabel>{t('form.titleForm.saisirCode')}  {t('form.legend.piecejoints')}</ControlLabel>

                                                <FormControl type="text" {...codeOTP}
                                                             onChange={(e) => {
                                                                 this.handleChangecodeOTP(e)
                                                             }}
                                                             value={this.state.codeOTP ? this.state.codeOTP : ""}
                                                             className={styles.datePickerFormControl} />
                                                {(SMSerror && SMSerror != true) &&
                                                <div className={styles.fieldRow}>
                                                    <i className="fa fa-exclamation-triangle"
                                                       aria-hidden="true">{SMSerror}</i>
                                                </div>}
                                            </Col>
                                            <Col xs="2" md="2">
                                                <Button onClick={(e) => {
                                                    this.handleResendOtp(data);
                                                }} bsStyle="primary" style={{ marginTop: '10%' }}> <i
                                                    className="fa fa-refresh" /> {t('form.buttons.renvoyerCode')} </Button>
                                            </Col>
                                        </Row>
                                        }
                                        {this.state.iSuccessContrat === true &&
                                        <Row>
                                            <Col xs="2" md="2">
                                                <Button
                                                    bsStyle="primary" style={{ marginRight: '10' }}
                                                    onClick={() => window.open(baseUrl + 'demandeSouscription/genererRIB/' + data.id)}
                                                ><i className="fa fa-file-pdf-o fa-4" /> {t('form.buttons.genererRib')}</Button>
                                            </Col>
                                        </Row>
                                        }
                                        {/**this.state.isGenerateReport === true &&
                                         <Row>
                                         <Col xs="2" md="2">
                                         <Button bsStyle="primary" style={{ marginRight: '10' }}
                                         onClick={() => window.open(baseUrl + 'demandeSouscription/createContratAbonnementPDF/' + data.id)}>
                                         <i className="fa fa-file-pdf-o fa-4" /> Imprimer le contrat moyens de paiement
                                         </Button>
                                         </Col>
                                         </Row>
                                         }
                                         {this.state.isGenerateReport === true &&
                                                    <Row>
                                                        <Col xs="2" md="2">
                                                            <Button bsStyle="primary" style={{ marginRight: '10' }}
                                                                onClick={() => window.open(baseUrl + 'demandeSouscription/createContratComptePaiement/' + data.id)}>
                                                                <i className="fa fa-file-pdf-o fa-4" /> Imprimer le contrat compte de paiement
                                                </Button>
                                                        </Col>
                                                    </Row>
                                                }
                                         {this.state.isGenerateReport === true &&
                                                    <Row>
                                                        <Col xs="4" md="4">
                                                            <Button bsStyle="primary" style={{ marginRight: '10' }}
                                                                onClick={() => window.open(baseUrl + 'demandeSouscription/createContratAbonnementGeneral')}>
                                                                <i className="fa fa-file-pdf-o fa-4" /> Imprimer les conditions générales
                                                </Button>
                                                        </Col>
                                                    </Row>
                                                }
                                         {this.state.isGenerateReport === true &&
                                                    <Row>
                                                        <Col xs="2" md="2">
                                                            <Button bsStyle="primary" style={{ marginRight: '0' }}
                                                                onClick={() => window.open(baseUrl + 'demandeSouscription/createFicheKYC/' + data.id)}>
                                                                <i className="fa fa-file-pdf-o fa-4" />  Imprimer la fiche KYC
                                                </Button>
                                                        </Col>
                                                    </Row>
                                         **/}
                                        {this.state.isGenerateReport === true &&
                                        <Row>
                                            <Col xs="2" md="2">
                                                <Button bsStyle="primary" style={{ marginRight: '10' }}
                                                        onClick={() => window.open(baseUrl + 'demandeSouscription/createContratAbonnementPDF/' + data.id)}>
                                                    <i className="fa fa-file-pdf-o fa-4" />{t('form.buttons.editerContrat')}
                                                </Button>
                                            </Col>
                                        </Row>
                                        }
                                        {this.state.isValidated && this.state.isValidated === true &&
                                        <div className="pull-right" style={{ paddingTop: '10px' }}>
                                            <Button
                                                disabled={SMSerror}
                                                onClick={handleSubmit(() => {
                                                    this.handleSave(data, values, data.id);
                                                })}

                                                bsStyle="primary"><i className="fa fa-check " />{t('form.buttons.terminer')}
                                            </Button>
                                        </div>
                                        }
                                        {(!this.state.isValidated || this.state.isValidated === false) &&
                                        <Row className="pull-right">
                                            <Col xs="12" md="12">
                                                <Button onClick={() => {
                                                    browserHistory.push(baseUrl + 'app/Historique');
                                                }} bsStyle="primary">
                                                    <i className="fa fa-check " /> {t('form.buttons.cancel')}
                                                </Button>
                                                <Button onClick={(e) => {
                                                    this.handleUpdate();
                                                }} bsStyle="primary">
                                                    <i className="fa fa-check " /> {t('form.buttons.update')}
                                                </Button>
                                                <Button onClick={(e) => {
                                                    this.handleRecapValidation(data);
                                                }} bsStyle="primary">
                                                    <i className="fa fa-check " /> {t('form.buttons.confirmerCreation')}
                                                </Button>
                                            </Col>
                                        </Row>
                                        }
                                    </div>
                                    }
                                </form>

                                {this.state.iSuccessContrat === true && this.state.isPieceJoint === true &&
                                <div>
                                    <legend>{t('form.legend.piecejoints')}</legend>
                                    <DownloadedFiles t={t} open={open} files={this.props.loadedContract}
                                                     handleSelect={this.handleSelect}
                                                     filesClickEvents={this.filesClickEvents}
                                                     handleFiles={this.handleFiles}
                                                     fichiersjoints={this.state.fichiersjoints}
                                                     disableALL={this.state.disableALL}
                                                     isConsultOnly={true}
                                    />
                                </div>
                                }
                            </fieldset>
                        </form>
                    </Row>

                </div>

                }


                {isSuccess && isSuccess === true && this.state.iSuccessContrat === false &&
                <div>
                    <Row>
                        <Col xs="12" md="12">
                            <h2>{t('form.titleForm.scanContrat')}</h2>
                        </Col>
                    </Row>
                    {contratErrorMsg && contratErrorMsg !== "" && this.state.iSuccessContrat === false &&
                    <div className="alert alert-danger">
                        {contratErrorMsg}
                    </div>
                    }
                    <form className="formContainer">
                        {/*<legend>{t('form.legend.piecejoints')}</legend>*/}
                        {/*<DownloadedFiles t={t} open={open} files={this.props.loadedDocuments}*/}
                        {/*handleSelect={this.handleSelect}*/}
                        {/*filesClickEvent={this.filesClickEvent}*/}
                        {/*disableALL={this.state.disableALL}*/}
                        {/*isConsultOnly={true}*/}
                        {/*/>*/}
                        <legend>{t('form.legend.uploadContrat')}</legend>
                        <div>
                            <DownloadedFiles t={t} open={open} files={null}
                                             handleSelect={this.handleSelect}
                                             filesClickEvents={this.filesClickEvents}
                                             handleFiles={this.handleFiles}
                                             fichiersjoints={this.state.fichiersjoints}
                                             disableALL={this.state.disableALL}
                                             isConsultOnly={false}
                            />
                        </div>
                        <div className="pull-right" style={{ paddingTop: '0px' }}>
                            <Button bsStyle="primary" onClick={() => {
                                this.handleSaveContrat()
                            }}>
                                <i className="fa fa-check " /> {t('form.buttons.confirmerSouscription')}
                            </Button>
                        </div>


                    </form>
                </div>
                }

                {this.state.iSuccessContrat === true &&
                <Row>
                    <Col xs="2" md="2">
                        <Button
                            bsStyle="primary" style={{ marginRight: '10' }}
                            onClick={() => window.open(baseUrl + 'demandeSouscription/genererRIB/' + data.id)}
                        ><i className="fa fa-file-pdf-o fa-4" /> {t('form.buttons.genererRib')}</Button>
                    </Col>
                </Row>
                }
            </div>
        );
    }
}
class DownloadedFiles extends Component {
    render() {
        const {
            t, handleSelect, open, files, isConsultOnly, formState, filesClickEvents, filesClickEvent, handleFiles, disableALL, styles,
            fichiersjoints, loadedDocuments, loadedContract
        } = this.props;
        return (

            <fieldset>
                <Row>
                    <Col xs={12} sm={12} md={6}>
                        {!isConsultOnly && /*File input that allows to list, add and delete files provided from the previous actions on this transaction*/

                        <Attachments title={"ajouter fichiers"} handleFiles={handleFiles} files={files}
                                     filesClickEvents={false} styles={styles} />
                        }
                        {isConsultOnly && /*File input that allows to list, add and delete files provided from the previous actions on this transaction*/

                        <Attachments title={"Télécharger les fichiers"} files={files} filesClickEvents={true} />

                        }


                    </Col>
                </Row>
            </fieldset>
        );
    }
}