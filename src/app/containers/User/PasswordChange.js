import React, {Component, PropTypes} from 'react';
import {
    Row,
    Col,
    FormControl,
    Button,
    Modal,
    ButtonGroup,
    ControlLabel
} from 'react-bootstrap';
import {connect} from 'react-redux';
import {reduxForm, initializeWithKey} from 'redux-form';
import {asyncConnect} from "redux-async-connect";
import {translate} from 'react-i18next';
import * as UserActions from './UserReducer';

export const validate = (values,props) => {
    const errors = {};
    if (!values.newPassword || !(values.newPassword.trim())) {
        errors.newPassword = props.t('form.message.champObligatoire');
    }
    if (!values.oldPassword || !(values.oldPassword.trim())) {
        errors.oldPassword = props.t('form.message.champObligatoire');
    } 
    if (!values.confirmationPassword || !(values.confirmationPassword.trim())) {
        errors.confirmationPassword = props.t('form.message.champObligatoire');
    } 
    return errors;
};

@translate(['password'], {wait: true})
@reduxForm({
    form: 'password',
    fields: ['newPassword','oldPassword','confirmationPassword'],
    validate,
},
state => (
    {
        initialValues: {
            newPassword:"",
            oldPassword:"",
            confirmationPassword:""
        },
    }
)
)

@connect(
    state => ({
        loadingUpdate:state.user.loadingUpdate,
        isSuccess:state.user.isSuccess,
        msg:state.user.msg,
    }), {...UserActions,initializeWithKey})


export default class PasswordChange extends Component {
    constructor() {
        super();
        this.getInitialState();
        this.handleChangeOld = this.handleChangeOld.bind(this);
        this.handleChangeNew = this.handleChangeNew.bind(this);
        this.handleChangeNewC = this.handleChangeNewC.bind(this);
    }
    getInitialState()
    {
        this.state = {
            oldPassword:'',
            newPassword:'',
            confirmationPassword:'',
            showModal:false,
        };
    }

    handleChangeOld(e)
    {
        this.setState({oldPassword:e.target.value});
    }
    handleChangeNew(e)
    {
        this.setState({newPassword:e.target.value});
    }
    handleChangeNewC(e)
    {
        this.setState({confirmationPassword:e.target.value});
    }
    async handleSave(dto){
        const logoutUrl = baseUrl + 'login/logout';
        if(dto.newPassword === dto.confirmationPassword){
            await this.props.saveChangePassword(dto);
            if(this.props.isSuccess === true){
                window.location.replace(logoutUrl);
            }
        }else{
            this.setState({showModal:true});
        }
        
    }
    
    render() {
        const {
            t,handleSubmit,loadingUpdate,isSuccess,msg,values,fields : {oldPassword,newPassword,confirmationPassword}
        } = this.props;
        const styles = require('../Souscription/Souscription.scss');
        const close = () => {
            this.setState({showModal: false});
        };
        return (
            <div>
                <Modal show={loadingUpdate} className="loadingModal" backdrop="static" keyboard={false}>
                    <Modal.Body>
                        <Row>
                            <Col xs={12} md={12}>
                                <div className="spinner">
                                    <span style={{fontSize: '11px'}}>{t('chargement.title')}</span>
                                </div>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
                <Row style={{marginBottom:'15px'}}>
                    <Col xs="12" md="12">
                        <h3>{t('form.title')}</h3>
                    </Col>
                </Row>
                {isSuccess===false &&
                        <div className="alert alert-danger">
                            {msg}
                        </div>
                }
                <form className="formContainer">
                    <Row className={styles.fieldRow}>
                                <Col xs="12" md="4">
                                    <ControlLabel>{t('form.label.ancienMotdepasse')}</ControlLabel>
                                    <FormControl type="password" {...oldPassword}
                                                onChange={(e) => {
                                                    this.handleChangeOld(e)
                                                }}
                                                value={this.state.oldPassword}
                                                className={styles.datePickerFormControl}/>
                                    {oldPassword.touched && oldPassword.error &&
                                        <div>
                                            <i className="fa fa-exclamation-triangle"
                                                aria-hidden="true">{oldPassword.error}</i>
                                        </div>
                                    }
                                </Col>
                    </Row>
                    <Row className={styles.fieldRow}>
                                <Col xs="12" md="4">
                                    <ControlLabel>{t('form.label.nouveauMotdepasse')} </ControlLabel>
                                    <FormControl type="password" {...newPassword}
                                                onChange={(e) => {
                                                    this.handleChangeNew(e)
                                                }}
                                                value={this.state.newPassword}
                                                className={styles.datePickerFormControl}/>
                                {newPassword.touched && newPassword.error &&
                                    <div>
                                        <i className="fa fa-exclamation-triangle"
                                            aria-hidden="true">{newPassword.error}</i>
                                    </div>
                                }
                                </Col>
                    </Row>
                    <Row className={styles.fieldRow}>
                                <Col xs="12" md="4">
                                    <ControlLabel>{t('form.label.confirmation')}  </ControlLabel>
                                    <FormControl type="password" {...confirmationPassword}
                                                onChange={(e) => {
                                                    this.handleChangeNewC(e)
                                                }}
                                                value={this.state.confirmationPassword}
                                                className={styles.datePickerFormControl}/>
                                    {confirmationPassword.touched && confirmationPassword.error &&
                                    <div>
                                        <i className="fa fa-exclamation-triangle"
                                        aria-hidden="true">{confirmationPassword.error}</i>
                                    </div>
                                    }
                                </Col>
                                
                    </Row>

                    <Row className={styles.fieldRow}>
                            <Col xs="12" md="12">
                                <Button onClick={handleSubmit(() => {
                                        this.handleSave(values);
                                        })}
                                        style={{float:'right'}}
                                        bsStyle="primary"><i className="fa fa-check " />
                                        {t('buttons.valider')}
                                </Button>
                            </Col>
                    </Row>

                </form> 
                <Modal  show={this.state.showModal}
                        onHide={close}
                        container={this}
                        aria-labelledby="contained-modal-title">
                            <Modal.Header closeButton>
                                <Modal.Title id="contained-modal-title">
                                    <div>{t('form.label.motdepasseIncorrect')}  </div>
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div>{t('form.label.msgErr')} </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <ButtonGroup
                                    className="pull-right" bsSize="small"
                                >
                                    <Button className={styles.ButtonPasswordStyle}
                                            onClick={() => close()}>{t('form.label.btt')}</Button>
                                </ButtonGroup>
                            </Modal.Footer>
                </Modal>
            </div>
        )}
}