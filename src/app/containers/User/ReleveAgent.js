import React, { Component } from 'react';
import { Row, Col, FormControl, Modal, ControlLabel, Tooltip, OverlayTrigger, ButtonGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import { reduxForm, initializeWithKey, change } from 'redux-form';
import { asyncConnect } from "redux-async-connect";
import { translate } from 'react-i18next';
import Button from "react-bootstrap-button-loader";
import Attachments from '../../components/Attachement/Attachments';
import * as AttachementActions from "../../components/Attachement/AttachementReducer";
import { isNullOrUndefined } from "util";
import Griddle from 'griddle-react';
import * as SouscriptionActions from "../Souscription/SouscriptionReducer";
import DatePicker from 'react-datepicker';
import moment from "moment";
import { reset } from 'redux-form';
import {AuthorizedComponent} from "react-router-role-authorization";
import * as UserActions from "./UserReducer";



class HeaderComponent extends Component {
    render() {
        return (<div
            style={{ fontSize: '12px', fontWeight: 'bold' }}
        >{this.props.displayName}</div>);
    }
}

class CenterComponent extends Component {
    render() {
        const url = this.props.data;
            return <div>{this.props.data}</div>;
    }
}

class CenterComponentCredit extends Component {
    render() {
        const url = this.props.data;
        if(this.props.data !== '')
            return <div style={{color : "#008000"}}>{'+ '+this.props.data}</div>;
        else
            return <div>{this.props.data}</div>;
    }
}
class CenterComponentDebit extends Component {
    render() {
        const url = this.props.data;
        if(this.props.data !== '')
            return <div style={{color : "#FF0000"}}>{'- '+this.props.data}</div>;
        else
            return <div>{this.props.data}</div>;
    }
}

@translate(['common'])
class NoDataComponent extends Component {
    render() {
        const { t } = this.props;
        return (
            <div className="noDataResult">
                <h4>{t('errors.noResult')}</h4>
            </div>
        );
    }
}

@connect(state => ({
    // DataClient: state.UpdateClientReducer.DataClient,
}), {
    // ...UpdateActions,initializeWithKey
})



@asyncConnect([{
    deferred: false,
    promise: ({params, location, store: {dispatch, getState}}) => {
        dispatch(UserActions.getMiniReleve(params.id)),
        dispatch(UserActions.loadUserFrontDetails())
    }
}])


@connect(
    state => ({
        user: state.user,
        // loadingUser: state.UpdateClientReducer.loading,
        // loading: state.AttachementReducer.loading,
        // DataClient: state.UpdateClientReducer.DataClient,
        listReleve: state.user.listReleve,
        agent : state.user.agent,
        totalTransaction:state.UpdateClientReducer.totalTransaction,
        // token: state.UpdateClientReducer.token,
        // nbMax:state.UpdateClientReducer.nbMax,
        userFrontDetails: state.user.userFrontDetails
    }), {  ...UserActions, SouscriptionActions, initializeWithKey })

@translate(['client'], { wait: true })
export default class ReleveAgent extends AuthorizedComponent {
    constructor(props) {
        super(props);
        this.userRoles = this.props.userFrontDetails.roles;
        this.notAuthorizedPath = baseUrl + 'app/AccessDenied';
        this.getInitialState();

        const _self = this;
        this.filesClickEvents = [_self.close, _self.deleteDoc];
        this.filesClickEvent = [];
    }

    getInitialState() {
        this.state = {
            showReleve:false,
        };
    }

    componentWillMount() {
        this.getInitialState();
    }

    render() {
        const {t, DataClient,userFrontDetails,agent, listReleve,totalTransaction,nbMax} = this.props;
        const styles = require('../Cashing/cashincashout.scss');

        return (
            <div>

                <Row>
                    <Col xs="12" md="12">
                        <h2>Mes Activités</h2>
                    </Col>
                </Row>


                <Row>
                    <fieldset style={{marginTop: '20px'}}>
                        <form className="formContainer">
                            <div className="table-responsive tablediv">
                                    <div>
                                        <Button bsStyle="primary"
                                                style={{float: 'right'}}
                                                disabled={ totalTransaction === 0}
                                                onClick={() => window.open(baseUrl + 'agent/createReleve?id=' + userFrontDetails.idUser)}
                                             >
                                            <i className="fa fa-file-pdf-o fa-4"/> Genérer PDF
                                        </Button>
                                    </div>
                                <Griddle
                                    results={listReleve}
                                    tableClassName="table"
                                    customNoDataComponent={NoDataComponent}
                                    useGriddleStyles={false}
                                    useCustomPagerComponent="true"
                                    resultsPerPage={20}
                                    //customPagerComponent={HistoriqueAgentPagination}
                                    columns={['date', 'label', 'debit', 'credit']}
                                    columnMetadata={
                                        [
                                            {
                                                columnName: 'date',
                                                displayName: 'Date Opération',
                                                customHeaderComponent: HeaderComponent,
                                                customComponent: CenterComponent,
                                                sortable: true
                                            },
                                            {
                                                columnName: 'label',
                                                displayName: 'Libellé de l\'opération' ,
                                                customHeaderComponent: HeaderComponent,
                                                customComponent: CenterComponent,
                                                sortable: false
                                            },
                                            {
                                                columnName: 'debit',
                                                displayName: 'Débit',
                                                customHeaderComponent: HeaderComponent,
                                                customComponent: CenterComponentDebit,
                                                sortable: false
                                            },
                                            {
                                                columnName: 'credit',
                                                displayName: 'Crédit',
                                                customHeaderComponent: HeaderComponent,
                                                customComponent: CenterComponentCredit,
                                                sortable: true
                                            }

                                        ]}
                                />
                            </div>
                        </form>
                    </fieldset>
                </Row>


            </div>
        )
    }
}
