const LOAD = "user/LOAD";
const LOAD_SUCCESS = "user/LOAD_SUCCESS";
const LOAD_FAIL = "user/LOAD_FAIL";

const UPDATE_PASSWORD = "user/UPDATE_PASSWORD";
const UPDATE_PASSWORD_SUCCESS = "user/UPDATE_PASSWORD_SUCCESS";
const UPDATE_PASSWORD_FAIL = "user/UPDATE_PASSWORD_FAIL";

const UPDATE_LANGUE = "user/UPDATE_LANGUE";
const UPDATE_LANGUE_SUCCESS = "user/UPDATE_LANGUE_SUCCESS";
const UPDATE_LANGUE_FAIL = "user/UPDATE_LANGUE_FAIL";

const MINI_RELEVE = "user/MINI_RELEVE";
const MINI_RELEVE_SUCCESS = "user/MINI_RELEVE_SUCCESS";
const MINI_RELEVE_FAIL = "user/MINI_RELEVE_FAIL";

const RELOAD = "user/RELOAD";

import _ from "lodash";

const initialState = {
  loaded: false,
  loadingUpdate: false,
  editing: {},
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      console.log("LOAD " + { ...action.result });
      return {
        ...state,
        loading: true,
      };
    case LOAD_SUCCESS:
      console.log("LOAD_SUCCESS " + { ...action.result });
      return {
        ...state,
        loading: false,
        userLoadSuccess: action.result.success,
        userFrontDetails: action.result.userFrontDetails,
        error: null,
      };
    case LOAD_FAIL:
      console.log("LOAD_FAIL " + { ...action.result });
      return {
        ...state,
        loading: false,
        loaded: false,
        loadedData: null,
        userLoadSuccess: false,
        userFrontDetails: null,
        error: action.error,
      };

    case UPDATE_PASSWORD:
      return {
        ...state,
        loadingUpdate: true,
        isSuccess: null,
        msg: null,
      };
    case UPDATE_PASSWORD_SUCCESS:
      return {
        ...state,
        loadingUpdate: false,
        isSuccess: action.result.success,
        msg: action.result.messageRetour,
      };
    case UPDATE_PASSWORD_FAIL:
      return {
        ...state,
        loadingUpdate: false,
        loaded: false,
        isSuccess: false,
        msg: action.result.messageRetour,
      };
    case UPDATE_LANGUE:
      return {
        ...state,
      };
    case UPDATE_LANGUE_SUCCESS:
      return {
        ...state,
        successUpdateLang: action.result.success,
        messageUpdateLang: action.result.messageRetour,
      };
    case UPDATE_LANGUE_FAIL:
      return {
        ...state,
        successUpdateLang: false,
        messageUpdateLang: action.result.messageRetour,
      };

    case MINI_RELEVE:
      return {
        ...state,
        loading: true,
      };
    case MINI_RELEVE_SUCCESS:
      if (action.result.success) {
        return {
          ...state,
          loading: false,
          listReleve: action.result.listReleve,
          success: action.result.success,
          totalTransaction: action.result.total,
          agent: action.result.agent,
        };
      } else {
        return {
          ...state,
          loading: false,
          listReleve: null,
          success: false,
          totalTransaction: 0,
        };
      }
    case MINI_RELEVE_FAIL:
      return {
        ...state,
        loading: false,
        listReleve: null,
        success: false,
        // errorMsg: action.result.messageRetour
      };
    case RELOAD:
      return {
        ...state,
        loadingUpdate: false,
      };

    default:
      return state;
  }
}

function objectToParams(object) {
  let str = "";
  for (const key in object) {
    if (str !== "") {
      str += "&";
    }
    str += key + "=" + encodeURIComponent(object[key]);
  }
  return str;
}

export function loadUserFrontDetails() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get("api/user"),
  };
}

export function isUserFrontDetailsLoaded(globalState) {
  return (
    globalState.user.userLoadSuccess === true &&
    globalState.user.userFrontDetails
  );
}

export function saveChangePassword(dto) {
  return {
    types: [UPDATE_PASSWORD, UPDATE_PASSWORD_SUCCESS, UPDATE_PASSWORD_FAIL],
    promise: (client) =>
      client.post("api/user/change/password", { data: objectToParams(dto) }),
  };
}

export function updateLangue(langue) {
  let dto = {};
  dto.langue = langue;
  return {
    types: [UPDATE_LANGUE, UPDATE_LANGUE_SUCCESS, UPDATE_LANGUE_FAIL],
    promise: (client) =>
      client.get("api/user/updateLangue", { params: objectToParams(dto) }),
  };
}

export function getMiniReleve(id) {
  console.log("mini releve");
  return {
    types: [MINI_RELEVE, MINI_RELEVE_SUCCESS, MINI_RELEVE_FAIL],
    promise: (client) => client.get("agent/releveListAgent?id=" + id),
  };
}

export function reload() {
  return {
    type: RELOAD,
  };
}
