import { createValidator,maxLength, required,champOb , phone,isValidAmount} from '../Commons/validationRules';

const CashInFormValidation = createValidator({
    montant:[required,isValidAmount],
    numeroCompte:[required,champOb],
    motif:[maxLength(30)]
});

export default CashInFormValidation;