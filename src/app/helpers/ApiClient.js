import superagent from "superagent";
import localStorage from "local-storage";

// import config from "../config";

const methods = ["get", "post", "put", "patch", "del"];

// export const baseUrl = "http://localhost:8080/EDPAGENT/";

export const baseUrl = "http://localhost:9988/adria/";
// export const baseUrl =
//   "https://edpagent.int.staging.bnpparibas.ma/AgentWallet2/";

function formatUrl(path) {
  return baseUrl + path;
}

export default class ApiClient {
  constructor(req) {
    methods.forEach(
      (method) =>
        (this[method] = (path, { params, data } = {}) =>
          new Promise((resolve, reject) => {
            const request = superagent[method](formatUrl(path));
            const csrfToken =
              localStorage.get("csrfToken") != null &&
              localStorage.get("csrfToken") != "" &&
              localStorage.get("csrfToken") != undefined
                ? localStorage.get("csrfToken")
                : null;

            if (params) {
              request.query(params);
            }

            console.log("Request Object :: " + JSON.stringify(req));

            if (__SERVER__ && req.get("cookie")) {
              request.set("cookie", req.get("cookie"));
            }

            if (method === "get" && data) {
              request.send(data);
            } else if (
              method === "post" &&
              data &&
              params &&
              params.type === "json"
            ) {
              request.set("_csrf", csrfToken);
              request.set("X-CSRF-TOKEN", csrfToken);
              request.set("Content-Type", "application/json");
              request.send(JSON.stringify(data));
            } else if (
              method === "post" &&
              data &&
              params &&
              params.type === "multipart/form-data"
            ) {
              request.set("X-CSRF-TOKEN", csrfToken);
              request.send(data);
            } else if (method === "post" && data) {
              console.log("hello from API_CLIENT 4");

              data = data + "&_csrf=" + csrfToken;
              request.set("X-CSRF-TOKEN", csrfToken);
              console.log("request :: " + request);

              request.send(data);
            }

            request.end((err, response = {}) => {
              console.log("response:: " + JSON.stringify(response));

              err
                ? reject(response.body || response.text || err)
                : resolve(response.body || response.text);
            });
            // request.end(function(err, res) {
            //   console.log("res.req._headers.cookie:: " + res.req._headers);
            // });
          }))
    );
  }
  empty() {}
}

// import superagent from "superagent";
// import localStorage from "local-storage";

// // Methods Accepted
// const methods = ["post", "get", "put", "patch", "del"];

// // URL microservices ADRIA

// //Liens Developpement
// //export const baseUrl =  "http://localhost:9080/";

// //Liens Recette
// //export const baseUrl = "http://51.91.193.154:9080/";

// //Liens Demo
// export const baseUrl = "http://localhost:8080/";
// //Liens Awb Egypt
// //export const baseUrl = "http://172.16.99.129:9443/";

// const clientId = "client-front";

// // Format URL
// export const formatUrl = (path) => {
//   return baseUrl + "app/" + path;
// };

// const keycloakTokenUrl = baseUrl;

// // const keycloakTokenUrl =
// // baseUrl + "auth/realms/standard-adria/protocol/openid-connect/token";

// const keycloakLogOutUrl =
//   baseUrl + "auth/realms/standard-adria/protocol/openid-connect/logout";

// // Export API
// export default class ApiClient {
//   constructor(req) {
//     methods.forEach(
//       (method) =>
//         (this[method] = (path, { params, data } = {}) =>
//           localStorage.get("isDemo")
//             ? loadDemo(method, path, params, data)
//             : new Promise((resolve, reject) => {
//                 if (data && path.indexOf("auth") >= 0) {
//                   let request = "";
//                   if (path.indexOf("login") >= 0) {
//                     request = superagent[method](keycloakTokenUrl);
//                   }
//                   if (path.indexOf("getStatusLogin") >= 0) {
//                     request = superagent[method](formatUrl(path));
//                   }
//                   // Login
//                   request.send("client_id=" + clientId);
//                   request.send("grant_type=password");
//                   request.send("baseUrl=" + baseUrl);
//                   request.send("username=" + data["username"]);
//                   request.send("password=" + data["password"]);
//                   request.send("codeBanque=" + data["codeBanque"]);
//                   request.send("langue=" + data["langue"]);
//                   request.send("typeProfil=CL");
//                   request.send("canal=Web");

//                   request.end((err, { body, text } = {}) =>
//                     err ? reject(body || text || err) : resolve(body || text)
//                   );
//                 } else if (path.indexOf("logout") >= 0) {
//                   this.logout();
//                 } else if (path.indexOf("refreshToken") >= 0) {
//                   this.refreshToken(data["contratId"]).then(
//                     (res) => {
//                       if (res.status === 200) {
//                         localStorage.set("token", res.body.access_token);
//                         localStorage.set(
//                           "refresh_token",
//                           res.body.refresh_token
//                         );
//                         resolve(res.body);
//                       } else {
//                         this.logout();
//                         localStorage.clear();
//                         window.location.replace(
//                           "/login/" + bankCode + "/" + lang
//                         );
//                         reject(res.body);
//                       }
//                     },
//                     (err) => {
//                       this.logout();
//                       localStorage.clear();
//                       reject(res.body);
//                     }
//                   );
//                 } else if (path.indexOf("logout") >= 0) {
//                   this.logout();
//                 } else {
//                   const request = superagent[method](formatUrl(path));
//                   if (params) {
//                     request.query(params);
//                   }
//                   if (
//                     method === "post" &&
//                     data &&
//                     params &&
//                     params.type === "multipart/form-data"
//                   ) {
//                     // Send POST file upload
//                     request.send(data);
//                   } else if (method === "post") {
//                     // Headers
//                     request.set(
//                       "Content-Type",
//                       "application/json;charset=UTF-8"
//                     );
//                     // Send Simple POST
//                     if (data) {
//                       // Others POST REQ with data wrapper
//                       let userDet = localStorage.get("userDetails");
//                       data = { userDetails: userDet, wrapper: data };
//                       request.send({ RequestAdriaDto: data });
//                     } else {
//                       let userDet = localStorage.get("userDetails");
//                       data = JSON.stringify({ userDetails: userDet });
//                       request.send(data);
//                     }
//                   }
//                   if (
//                     localStorage.get("token") != null &&
//                     localStorage.get("token") != "" &&
//                     localStorage.get("token") != undefined
//                   ) {
//                     request.set(
//                       "Authorization",
//                       "Bearer " + localStorage.get("token")
//                     );
//                   }

//                   request.end((err, { body, text, status } = {}) => {
//                     if (err) {
//                       if (
//                         status === 401 ||
//                         status === 403 ||
//                         status == undefined
//                       ) {
//                         this.refreshToken().then(
//                           (res) => {
//                             if (res.status === 200) {
//                               console.log("after refresh res ");
//                               localStorage.set("token", res.body.access_token);
//                               localStorage.set(
//                                 "refresh_token",
//                                 res.body.refresh_token
//                               );
//                               request.set(
//                                 "Authorization",
//                                 "Bearer " + res.body.access_token
//                               );
//                               request.end((err, { body, text } = {}) =>
//                                 err
//                                   ? reject(body || text || err)
//                                   : resolve(body || text)
//                               );
//                             } else {
//                               this.logout();
//                               localStorage.clear();
//                               window.location.replace(
//                                 "/login/" + bankCode + "/" + lang
//                               );
//                               reject(body || text || err);
//                             }
//                           },
//                           (err) => {
//                             //this.logout();
//                             localStorage.clear();
//                             //window.location.replace("/login/"+bankCode+"/"+lang);
//                             reject(body || text || err);
//                           }
//                         );
//                       } else {
//                         reject(body || text || err);
//                       }
//                     } else {
//                       resolve(body || text);
//                     }
//                   });
//                 }
//               }))
//     );
//   }

//   refreshToken = (contratId = "") => {
//     return superagent
//       .post(keycloakTokenUrl)
//       .send("client_id=" + clientId)
//       .send("baseUrl=" + baseUrl)
//       .send("contratId=" + contratId)
//       .send("grant_type=refresh_token")
//       .send("token=" + localStorage.get("token"))
//       .send("refresh_token=" + localStorage.get("refresh_token"));
//   };

//   logout = () => {
//     localStorage.set("isDemo", false);
//     const request = superagent.post(formatUrl("contrats/auth/logout"));
//     request.set("Content-Type", "application/json;charset=UTF-8");
//     request.set("Authorization", "Bearer " + localStorage.get("token"));
//     const userDet = localStorage.get("userDetails");
//     const data = JSON.stringify({ userDetails: userDet });
//     request.send(data);
//     request.then(
//       (res) => {
//         superagent
//           .post(keycloakLogOutUrl)
//           .send("client_id=" + clientId)
//           .send("refresh_token=" + localStorage.get("refresh_token"))
//           .then(
//             (res) => {
//               localStorage.clear();
//               window.location.replace("/login/" + bankCode + "/" + lang);
//               reject(body || text || err);
//             },
//             (err) => {
//               localStorage.clear();
//               window.location.replace("/login/" + bankCode + "/" + lang);
//               reject(body || text || err);
//             }
//           );
//       },
//       (err) => {
//         localStorage.clear();
//         window.location.replace("/login/" + bankCode + "/" + lang);
//         reject(body || text || err);
//       }
//     );
//   };
// }

// export const loadDemo = (method, path, params, data) => {
//   let newPath = path.split("?")[0];

//   let response = {};
//   try {
//     response = require("./" + newPath + ".json");
//   } catch (err) {
//     console.log(
//       "The file : " +
//         newPath +
//         ".json Not Found create One to Mock the request : " +
//         path
//     );
//   }
//   /** SHOW REPONSE IN CONSOLE **/
//   console.log("REQUEST :::::: ./" + newPath);
//   console.log(response);

//   return new Promise((resolve, reject) => {
//     resolve(response);
//   });
// };
