import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div className="well text-center">
        Mentions légales - Copyright DAMANE CASH © 2020.
      </div>
    );
  }
}
