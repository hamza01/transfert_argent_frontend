import React, { Component, PropTypes } from "react";
import { connect } from "react-redux";
import { browserHistory, IndexLink } from "react-router";
import { LinkContainer } from "react-router-bootstrap";
import { logout } from "redux/modules/auth";
import { push } from "react-router-redux";
import {
  Navbar,
  Nav,
  NavItem,
  NavDropdown,
  MenuItem,
  Grid,
  Row,
  Col,
  DropdownButton,
  ButtonToolbar,
} from "react-bootstrap/lib";
import FontAwesome from "react-fontawesome";
import MenuOffline from "./MenuOffline";

export default class HeaderOffline extends Component {
  render() {
    const styles = require("./Header.scss");
    const title = <i className="fa fa-list" />;
    return (
      <div className={styles.appHeader}>
        <div className={styles.notifbar}>
          <link
            href="https://fonts.googleapis.com/css?family=Open+Sans"
            rel="stylesheet"
          />
          <div className="container"></div>
        </div>
        <Navbar fixedTop>
          <Navbar.Header>
            <Navbar.Brand className={styles.brand} />
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse eventkey={0}>
            <MenuOffline />
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
