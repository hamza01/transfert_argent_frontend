// Global bank
const bank = process.env.theme || "00000";
const path = require("path");
const autoprefixer = require("autoprefixer");
const webpack = require("webpack");

const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const WebpackIsomorphicToolsPlugin = require("webpack-isomorphic-tools/plugin");
const webpackIsomorphicToolsPlugin = new WebpackIsomorphicToolsPlugin(
  require("./webpack-isomorphic-tools")
);
// Path config
const assetsPath = path.resolve(__dirname, "../public/", bank);
// Export module webpack
module.exports = {
  target: "web",
  devtool: "cheap-module-eval-source-map",
  performance: {
    hints: "warning",
    maxAssetSize: 2004800000,
    maxEntrypointSize: 2004800000,
  },
  context: path.resolve(__dirname, "."),
  entry: [
    "@babel/polyfill",
    `bootstrap-loader/lib/bootstrap.loader?configFilePath=${path.resolve(
      __dirname,
      "../build/" + bank + "/",
      ".bootstraprc"
    )}!bootstrap-loader/no-op.js`,
    "font-awesome-webpack!../src/themes/" + bank + "/font-awesome.config.js",
    "../src/index.js",
  ],
  //   output: {
  //     path: assetsPath,
  //     filename: '[name].js',
  //     publicPath: '/'+ bank +"/"
  //   },
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "../dist"),
    publicPath: "",
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        exclude: /node_modules/,
        use: "babel-loader",
      },
      {
        test: /\.css$/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader",
          "css-loader?modules&sourceMap&localIdentName=[local]___[hash:base64:5]",
          "sass-loader?outputStyle=expanded&sourceMap",
        ],
      },
      {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "url-loader",
            options: { limit: 10002400, mimetype: "application/font-woff" },
          },
        ],
      },
      {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "url-loader",
            options: { limit: 10002400, mimetype: "application/font-woff" },
          },
        ],
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "url-loader",
            options: { limit: 10002400, mimetype: "application/octet-stream" },
          },
        ],
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "url-loader",
            options: { limit: 10002400, mimetype: "application/octet-stream" },
          },
        ],
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "url-loader",
            options: { limit: 10002400, mimetype: "image/svg+xml" },
          },
        ],
      },
      {
        test: webpackIsomorphicToolsPlugin.regular_expression("images"),
        use: [{ loader: "url-loader", options: { limit: 10002400 } }],
      },
      // Bootstrap 3
      {
        test: /bootstrap-sass\/assets\/javascripts\//,
        use: "imports-loader?jQuery=jquery",
      },
    ],
  },
  resolve: {
    modules: ["src", "node_modules"],
    extensions: [".js", ".jsx", ".mjs"],
  },
  devServer: {
    contentBase: path.join(__dirname, "../dist"),
    hot: false,
    host: "127.0.0.1",
    port: 3000,
    open: false,
    historyApiFallback: true,
  },
  node: {
    fs: "empty",
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.LoaderOptionsPlugin({
      postcss: [autoprefixer],
    }),
    new HtmlWebpackPlugin({
      title: "hello world",
      filename: "index.html",
      template: path.resolve(__dirname, "../index.hbs"),
      description: "webpack is awsome",
    }),
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": '"development"',
      __APP_NAME__: JSON.stringify(process.env.appName),
      __CLIENT__: true,
      __SERVER__: false,
      __DEVELOPMENT__: true,
      __DEVTOOLS__: true,
    }),
  ],
};
